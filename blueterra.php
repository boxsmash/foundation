    <?php

    /*
      Plugin Name: BlueTerra Products
      Plugin URI: http://c2ginteractive.com
      Description: Creates BlueTerra product type and related taxonomy.
      Version: 1.0.0
      Author: c2g interactive
      Author URI: http://c2ginteractive.com
      License: Private.
     */

    function blueterra_product_activation() {
        if (get_option("medium_crop") == false) {
            add_option("medium_crop", "1");
        } else {
            update_option("medium_crop", "1");
        }
    }

    function blueterra_product_deactivate() {
    // Deactivation code here...
    }

    add_action('init', 'create_blueterra_product_type');
    add_action('init', 'create_blueterra_product_taxonomy');
    add_action('pre_get_posts', 'my_change_sort_order');

    function my_change_sort_order($query) {
        if (is_archive()):
            //If you wanted it for the archive of a custom post type use: is_post_type_archive( $post_type )*/
            //Set the order ASC or DESC
            $query->set('order', 'ASC');
            //Set the orderby
            $query->set('orderby', 'title');
        endif;
    }

    ;

    add_filter('get_previous_post_where', 'filter_prev_post_where');
    add_filter('get_next_post_where', 'filter_next_post_where');
    add_filter('get_previous_post_sort', 'filter_prev_post_sort');
    add_filter('get_next_post_sort', 'filter_next_post_sort');

    function filter_prev_post_where($where, $in_same_cat = false, $excluded_categories = '') {
        global $post, $wpdb;
        $join = '';
        $posts_in_ex_cats_sql = '';
        if ($in_same_cat || !empty($excluded_categories)) {
            $join = " INNER JOIN $wpdb->term_relationships AS tr ON p.ID = tr.object_id INNER JOIN $wpdb->term_taxonomy tt ON tr.term_taxonomy_id = tt.term_taxonomy_id";
            if ($in_same_cat) {
                $cat_array = wp_get_object_terms($post->ID, 'category', array('fields' => 'ids'));
                $join .= " AND tt.taxonomy = 'category' AND tt.term_id IN (" . implode(',', $cat_array) . ")";
            }
            $posts_in_ex_cats_sql = "AND tt.taxonomy = 'category'";
            if (!empty($excluded_categories)) {
                if (!is_array($excluded_categories)) {
                    // back-compat, $excluded_categories used to be IDs separated by " and "
                    if (strpos($excluded_categories, ' and ') !== false) {
                        _deprecated_argument(__FUNCTION__, '3.3', sprintf(__('Use commas instead of %s to separate excluded categories.'), "'and'"));
                        $excluded_categories = explode(' and ', $excluded_categories);
                    } else {
                        $excluded_categories = explode(',', $excluded_categories);
                    }
                }
                $excluded_categories = array_map('intval', $excluded_categories);
                if (!empty($cat_array)) {
                    $excluded_categories = array_diff($excluded_categories, $cat_array);
                    $posts_in_ex_cats_sql = '';
                }
                if (!empty($excluded_categories)) {
                    $posts_in_ex_cats_sql = " AND tt.taxonomy = 'category' AND tt.term_id NOT IN (" . implode($excluded_categories, ',') . ')';
                }
            }
        }
        $where = $wpdb->prepare("WHERE p.post_title < %s AND p.post_type = %s AND p.post_status = 'publish' $posts_in_ex_cats_sql", $post->post_title, $post->post_type);
        return $where;
    }

    function filter_next_post_where($where, $in_same_cat = false, $excluded_categories = '') {
        global $post, $wpdb;
        $join = '';
        $posts_in_ex_cats_sql = '';
        if ($in_same_cat || !empty($excluded_categories)) {
            $join = " INNER JOIN $wpdb->term_relationships AS tr ON p.ID = tr.object_id INNER JOIN $wpdb->term_taxonomy tt ON tr.term_taxonomy_id = tt.term_taxonomy_id";
            if ($in_same_cat) {
                $cat_array = wp_get_object_terms($post->ID, 'category', array('fields' => 'ids'));
                $join .= " AND tt.taxonomy = 'category' AND tt.term_id IN (" . implode(',', $cat_array) . ")";
            }
            $posts_in_ex_cats_sql = "AND tt.taxonomy = 'category'";
            if (!empty($excluded_categories)) {
                if (!is_array($excluded_categories)) {
                    // back-compat, $excluded_categories used to be IDs separated by " and "
                    if (strpos($excluded_categories, ' and ') !== false) {
                        _deprecated_argument(__FUNCTION__, '3.3', sprintf(__('Use commas instead of %s to separate excluded categories.'), "'and'"));
                        $excluded_categories = explode(' and ', $excluded_categories);
                    } else {
                        $excluded_categories = explode(',', $excluded_categories);
                    }
                }
                $excluded_categories = array_map('intval', $excluded_categories);
                if (!empty($cat_array)) {
                    $excluded_categories = array_diff($excluded_categories, $cat_array);
                    $posts_in_ex_cats_sql = '';
                }
                if (!empty($excluded_categories)) {
                    $posts_in_ex_cats_sql = " AND tt.taxonomy = 'category' AND tt.term_id NOT IN (" . implode($excluded_categories, ',') . ')';
                }
            }
        }
        $where = $wpdb->prepare("WHERE p.post_title > %s AND p.post_type = %s AND p.post_status = 'publish' $posts_in_ex_cats_sql", $post->post_title, $post->post_type);
        return $where;
    }

    function filter_prev_post_sort($sort) {
        $sort = "ORDER BY post_title DESC LIMIT 1";
        return $sort;
    }

    function filter_next_post_sort($sort) {
        $sort = "ORDER BY p.post_title ASC LIMIT 1";
        return $sort;
    }

    function create_blueterra_product_type() {
        register_post_type('blueterra-product', array(
            'labels' => array(
                'name' => __('BlueTerra Products'),
                'singular_name' => __('BlueTerra Product')
            ),
            'capability_type' => 'post',
            'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'blueterra-products'),
                )
        );
    }

    function create_blueterra_product_taxonomy() {

    // create a new taxonomy
        register_taxonomy(
                'blueterra-material', 'blueterra-product', array(
            'label' => __('Material'),
            'sort' => true,
            'hierarchical' => true,
            'args' => array('orderby' => 'term_order'),
            'rewrite' => array('slug' => 'blueterra-material')
                )
        );

        $parent_term = term_exists('blueterra-material', 'blueterra-product'); // array is returned if taxonomy is given
        $parent_term_id = $parent_term['term_id']; // get numeric term id
        wp_insert_term(
                'Granite', // the term
                'blueterra-material', // the taxonomy
                array(
            'slug' => 'blueterra-granite',
            'parent' => $parent_term_id
                )
        );
        wp_insert_term(
                'Marble', // the term
                'blueterra-material', // the taxonomy
                array(
            'slug' => 'blueterra-marble',
            'parent' => $parent_term_id
                )
        );
        wp_insert_term(
                'Pebble', // the term
                'blueterra-material', // the taxonomy
                array(
            'slug' => 'blueterra-pebble',
            'parent' => $parent_term_id
                )
        );
        wp_insert_term(
                'Quartz', // the term
                'blueterra-material', // the taxonomy
                array(
            'slug' => 'blueterra-quartz',
            'parent' => $parent_term_id
                )
        );


    //*****************************************************************
    // Create Color Taxonomy
    //*****************************************************************
        register_taxonomy(
                'blueterra-color', 'blueterra-product', array(
            'label' => __('Color'),
            'sort' => true,
            'hierarchical' => true,
            'args' => array('orderby' => 'term_order'),
            'rewrite' => array('slug' => 'blueterra-color')
                )
        );

    // Create Blue Color
        $parent_term = term_exists('blueterra-color', 'blueterra-product'); // array is returned if taxonomy is given
        $parent_term_id = $parent_term['term_id']; // get numeric term id
        $return = wp_insert_term(
                'Blue', // the term
                'blueterra-color', // the taxonomy
                array(
            'slug' => 'blueterra-blue',
            'parent' => $parent_term_id
                )
        );
        if (!is_wp_error($return)) {
            $new_term_id = $return['term_id'];
            wp_insert_term(
                    'Lt Blue', // the term
                    'blueterra-color', // the taxonomy
                    array(
                'slug' => 'blueterra-lt-blue-pool-surface',
                'parent' => $new_term_id
                    )
            );
            wp_insert_term(
                    'Med Blue', // the term
                    'blueterra-color', // the taxonomy
                    array(
                'slug' => 'blueterra-med-blue-pool-surface',
                'parent' => $new_term_id
                    )
            );
            wp_insert_term(
                    'Dark Blue', // the term
                    'blueterra-color', // the taxonomy
                    array(
                'slug' => 'blueterra-dark-blue-pool-surface',
                'parent' => $new_term_id
                    )
            );
        }

    // Create Green Color
        $return = wp_insert_term(
                'Green', // the term
                'blueterra-color', // the taxonomy
                array(
            'slug' => 'blueterra-green',
            'parent' => $parent_term_id
                )
        );
        if (!is_wp_error($return)) {
            $new_term_id = $return['term_id'];
            wp_insert_term(
                    'Lt Green', // the term
                    'blueterra-color', // the taxonomy
                    array(
                'slug' => 'blueterra-lt-green-pool-surface',
                'parent' => $new_term_id
                    )
            );
            wp_insert_term(
                    'Med Green', // the term
                    'blueterra-color', // the taxonomy
                    array(
                'slug' => 'blueterra-med-green-pool-surface',
                'parent' => $new_term_id
                    )
            );
            wp_insert_term(
                    'Dark Green', // the term
                    'blueterra-color', // the taxonomy
                    array(
                'slug' => 'blueterra-dark-green-pool-surface',
                'parent' => $new_term_id
                    )
            );
        }


    //Create Grey Color
        $return = wp_insert_term(
                'Grey', // the term
                'blueterra-color', // the taxonomy
                array(
            'slug' => 'blueterra-grey',
            'parent' => $parent_term_id
                )
        );
        if (!is_wp_error($return)) {
            $new_term_id = $return['term_id'];
            wp_insert_term(
                    'Lt Grey', // the term
                    'blueterra-color', // the taxonomy
                    array(
                'slug' => 'blueterra-lt-grey-pool-surface',
                'parent' => $new_term_id
                    )
            );
            wp_insert_term(
                    'Med Grey', // the term
                    'blueterra-color', // the taxonomy
                    array(
                'slug' => 'blueterra-med-grey-pool-surface',
                'parent' => $new_term_id
                    )
            );
            wp_insert_term(
                    'Dark Grey', // the term
                    'blueterra-color', // the taxonomy
                    array(
                'slug' => 'blueterra-dark-grey-pool-surface',
                'parent' => $new_term_id
                    )
            );
        }


    // Create Tan Color
        $return = wp_insert_term(
                'Tan', // the term
                'blueterra-color', // the taxonomy
                array(
            'slug' => 'blueterra-tan',
            'parent' => $parent_term_id
                )
        );
        if (!is_wp_error($return)) {
            $new_term_id = $return['term_id'];
            wp_insert_term(
                    'Lt Tan', // the term
                    'blueterra-color', // the taxonomy
                    array(
                'slug' => 'blueterra-lt-tan-pool-surface',
                'parent' => $new_term_id
                    )
            );
            wp_insert_term(
                    'Med Tan', // the term
                    'blueterra-color', // the taxonomy
                    array(
                'slug' => 'blueterra-med-tan-pool-surface',
                'parent' => $new_term_id
                    )
            );
            wp_insert_term(
                    'Dark Tan', // the term
                    'blueterra-color', // the taxonomy
                    array(
                'slug' => 'blueterra-dark-tan-pool-surface',
                'parent' => $new_term_id
                    )
            );
        }


        register_taxonomy(
                'blueterra-cost', 'blueterra-product', array(
            'label' => __('Cost'),
            'sort' => true,
            'hierarchical' => true,
            'args' => array('orderby' => 'term_order'),
            'rewrite' => array('slug' => 'blueterra-cost')
                )
        );

        $parent_term = term_exists('blueterra-cost', 'blueterra-product'); // array is returned if taxonomy is given
        $parent_term_id = $parent_term['term_id']; // get numeric term id
        wp_insert_term(
                '$', // the term
                'blueterra-cost', // the taxonomy
                array(
            'slug' => 'blueterra-cost-1',
            'parent' => $parent_term_id
                )
        );
        wp_insert_term(
                '$$', // the term
                'blueterra-cost', // the taxonomy
                array(
            'slug' => 'blueterra-cost-2',
            'parent' => $parent_term_id
                )
        );
        wp_insert_term(
                '$$$', // the term
                'blueterra-cost', // the taxonomy
                array(
            'slug' => 'blueterra-cost-3',
            'parent' => $parent_term_id
                )
        );
        wp_insert_term(
                '$$$$', // the term
                'blueterra-cost', // the taxonomy
                array(
            'slug' => 'blueterra-cost-4',
            'parent' => $parent_term_id
                )
        );

        register_taxonomy(
                'blueterra-texture', 'blueterra-product', array(
            'label' => __('Texture'),
            'sort' => true,
            'hierarchical' => true,
            'args' => array('orderby' => 'term_order'),
            'rewrite' => array('slug' => 'blueterra-texture')
                )
        );

        $parent_term = term_exists('blueterra-texture', 'blueterra-product'); // array is returned if taxonomy is given
        $parent_term_id = $parent_term['term_id']; // get numeric term id
        wp_insert_term(
                'Polished', // the term
                'blueterra-texture', // the taxonomy
                array(
            'slug' => 'polished',
            'parent' => $parent_term_id
                )
        );
        wp_insert_term(
                'Smooth', // the term
                'blueterra-texture', // the taxonomy
                array(
            'slug' => 'smooth',
            'parent' => $parent_term_id
                )
        );
        wp_insert_term(
                'Slightly Textured', // the term
                'blueterra-texture', // the taxonomy
                array(
            'slug' => 'slightly-textured',
            'parent' => $parent_term_id
                )
        );
        wp_insert_term(
                'Smooth Pebble', // the term
                'blueterra-texture', // the taxonomy
                array(
            'slug' => 'smooth-pebble',
            'parent' => $parent_term_id
                )
        );
    }

    function blueterra_product_filter_handler($atts) {
        global $wpdb;
        global $post;
        global $_wp_additional_image_sizes;

        static $teaser_count = 0;
        $teaser_count++;

        $result = "";

        /* content => 'title', 'author', 'excerpt', 'content', 'image', 'teaser' */
        /*
          Post (Post Type: 'post')
          Page (Post Type: 'page')
          Attachment (Post Type: 'attachment')
          Revision (Post Type: 'revision')
          Navigation menu (Post Type: 'nav_menu_item')
         */

        extract(shortcode_atts(array(
            'include' => 'title,thumbnail,excerpt',
            'type' => 'blueterra-product',
            'color' => '',
            'material' => '',
            'cost' => '',
            'children' => TRUE,
            'debug' => FALSE,
            'count' => 1000,
            'order' => 'post_title',
            'words' => 20
                        ), $atts, 'blueterra-product-filter'));

        $delimiters = array(
            ",",
            ".",
            "|",
            ":"
        );

        $material = explode($delimiters[0], str_replace($delimiters, $delimiters[0], strtolower($material)));
        $material_whereclause = "";
        foreach ($material as $item) {
            if ($item != '')
                $material_whereclause = $material_whereclause . " AND lower(name) LIKE '" . trim($item) . "'";
        }

        $color = explode($delimiters[0], str_replace($delimiters, $delimiters[0], strtolower($color)));
        $color_whereclause = "";
        foreach ($color as $item) {
            if ($item != '')
                $color_whereclause = $color_whereclause . " AND lower(name) LIKE '" . trim($item) . "'";
        }

        $cost = explode($delimiters[0], str_replace($delimiters, $delimiters[0], strtolower($cost)));
        $cost_whereclause = "";
        foreach ($cost as $item) {
            if ($item != '')
                $cost_whereclause = $cost_whereclause . " AND lower(name) LIKE '" . trim($item) . "'";
        }


        $include = explode($delimiters[0], str_replace($delimiters, $delimiters[0], strtolower($include)));
        $type = explode($delimiters[0], str_replace($delimiters, $delimiters[0], strtolower($type)));


        $sql = "
            SELECT DISTINCT wp_posts.*
            FROM   wp_posts";

    //Determine products that match specified material
        $sql = $sql . "
            JOIN (SELECT wp_posts.id
                    FROM wp_posts";
        if ($material_whereclause <> '')
            $sql = $sql . "
                    JOIN wp_term_relationships
                      ON wp_term_relationships.object_id = wp_posts.id
                    JOIN wp_term_taxonomy
                      ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
                         AND wp_term_taxonomy.taxonomy = 'blueterra-material'
                    JOIN wp_terms
                      ON wp_term_taxonomy.term_id = wp_terms.term_id " . $material_whereclause;
        $sql = $sql . ") as material
              ON wp_posts.id = material.id";

    //Determine products that match specified color
        $sql = $sql . "
            JOIN (SELECT wp_posts.id
                    FROM wp_posts";
        if ($color_whereclause <> '')
            $sql = $sql . "
                    JOIN wp_term_relationships
                      ON wp_term_relationships.object_id = wp_posts.id
                    JOIN wp_term_taxonomy
                      ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
                         AND wp_term_taxonomy.taxonomy = 'blueterra-material'
                    JOIN wp_terms
                      ON wp_term_taxonomy.term_id = wp_terms.term_id " . $color_whereclause;
        $sql = $sql . ") as color
              ON wp_posts.id = color.id";

        //Determine products that match specified cost
        $sql = $sql . "
            JOIN (SELECT wp_posts.id
                    FROM wp_posts";
        if ($color_whereclause <> '')
            $sql = $sql . "
                    JOIN wp_term_relationships
                      ON wp_term_relationships.object_id = wp_posts.id
                    JOIN wp_term_taxonomy
                      ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
                         AND wp_term_taxonomy.taxonomy = 'blueterra-material'
                    JOIN wp_terms
                      ON wp_term_taxonomy.term_id = wp_terms.term_id " . $cost_whereclause;
        $sql = $sql . ") as cost
              ON wp_posts.id = cost.id";

        // Order by clause
        $sql = $sql . "
            WHERE wp_posts.post_type = 'blueterra-product'
            ORDER BY " . $order;

        if ($debug == TRUE)
            return $result = $sql;

        $teaser_list_count = 0;
        $class_prefix = "blueterra_product";

        $pageposts = $wpdb->get_results($sql, OBJECT);

        if ($pageposts) {

            $result .= '<div id="blueterra_product_list_' . $teaser_count . '" class="blueterra_product_list">';

            $numItems = count($pageposts);
            $i = 0;
            foreach ($pageposts as $pageChild) {
                $i++;
                $firstitem = ($i == 1);
                $lastitem = ($i == $numItems OR $i == $count);

                if (++$teaser_list_count > $count)
                    break;

                $result .= "<div id=\"blueterra_product_listitem_" . $teaser_list_count . "\" class=\"blueterra_product_listitem";
                if ($firstitem)
                    $result .= " first_item";

                if ($lastitem)
                    $result .= " last_item";

                $result .= "\">";

                $post = $pageChild;
                setup_postdata($post);

                foreach ($include as $DisplayElement) {

                    if ($DisplayElement == "title") {
                        $result .= "\n<div class=\"" . $class_prefix . "_title\">\n";
                        $result .= "<a href=\"" . get_permalink($pageChild->ID) . "\" rel=\"bookmark\" title=\"" . $pageChild->post_title . "\">" . $pageChild->post_title . "</a>";
                        $result .= "\n</div>\n";
                    } elseif ($DisplayElement == "thumbnail") {
                        $result .= "\n<div class=\"" . $class_prefix . "_thumbnail\">\n";
                        $thumbnail_html = get_the_post_thumbnail($pageChild->ID, 'thumbnail');
                        if ($thumbnail_html == "") {
                            $thumbnail_html = '<img src="/img_not_found.jpg" alt="job" height="150" width="150">';
                        }
                        $result .= "<a href=\"" . get_permalink($pageChild->ID) . "\" rel=\"bookmark\" title=\"" . $pageChild->post_title . "\">" . $thumbnail_html . "</a>";
                        $result .= "\n</div>\n";
                    } elseif ($DisplayElement == "excerpt") {
                        $result .= "\n<div class=\"" . $class_prefix . "_excerpt\">\n";
                        $result .= wp_trim_words(get_the_excerpt(), $words, '...');
                        $result .= "\n</div>\n";
                    } elseif ($DisplayElement == "quickview") {
                        $result .= "<div class=\"" . $class_prefix . "_quickview\">\n";
                        $result .= apply_filters('the_content', get_the_content());
                        $result .= "\n</div>\n";
                    }
                }
                $result .= "\n</div>\n";
            }

            $result .= "\n</div>\n";

            wp_reset_postdata();
        }

        return $result;
    }

    function blueterra_product_watercolor_handler($atts) {
        global $wpdb;
        global $post;
        global $_wp_additional_image_sizes;

        static $teaser_count = 0;
        $teaser_count++;

        $result = "";

        $result = "
            <div class=\"blueterra_watercolor\">
                    <div id=\"blueterra_watercolor_water\">
                    </div>
                    <div id=\"blueterra_watercolor_plaster\">
                    </div>
            </div>
            ";

        return $result;
    }

    register_activation_hook(__FILE__, 'blueterra_product_activation');
    register_deactivation_hook(__FILE__, 'blueterra_product_deactivate');

    add_shortcode('blueterra_product_filter', 'blueterra_product_filter_handler');
    add_shortcode('blueterra_product_watercolor', 'blueterra_product_watercolor_handler');
    ?>
