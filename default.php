<?php

function thesis_foundation_defaults() {
	return array (
  'css' => '* {
    margin: 0;
    padding: 0;
    word-wrap: break-word
}

html {
    -ms-text-size-adjust: 100%;
    -webkit-text-size-adjust: 100%
}

body {
    font-family: $font;
    font-size: $f_text;
    font-weight: 300;
    line-height: 1.618;
    color: $text1;
    background-color: $color3
}

.hidden {
    display: none
}

h1,
h2,
h3,
h4,
h5,
h6,
p {
    line-height: 1.45
}

h1 {
    font-size: 3em;
    font-weight: 400;
    line-height: 1.45
}

h2 {
    font-size: 2em;
    font-weight: 400;
    line-height: 1.45;
    margin-top: $x_3over2;
    margin-bottom: $x_half;
    color: $headline_color
}

h3 {
    font-size: 1em;
    font-weight: 700;
    color: $headline_color
}

h4 {
    font-weight: 400
}

h5 {
    font-weight: 400
}

h6 {
    font-weight: 400
}

table {
    border-spacing: 0;
    border-collapse: collapse
}

img,
abbr,
acronym,
fieldset {
    border: 0
}

code {
    line-height: 1em
}

pre {
    clear: both;
    overflow: auto;
    word-wrap: normal;
    -moz-tab-size: 4;
    -o-tab-size: 4;
    tab-size: 4
}

sub,
sup {
    line-height: .5em
}

img,
.wp-caption {
    max-width: 100%;
    height: auto
}

.wp-caption .wp-caption-text .wp-smiley {
    display: inline
}

.sidebar ul ul,
.sidebar ul ol,
.sidebar ol ul,
.sidebar ol ol,
.wp-caption p,
.sidebar .post_excerpt p {
    margin-bottom: 0
}

iframe,
video,
embed,
object {
    display: block;
    max-width: 100%
}

img {
    display: block
}

.left,
.alignleft,
img[align=\\\'left\\\'] {
    display: block;
    float: left
}

.right,
.alignright,
img[align=\\\'right\\\'] {
    display: block;
    float: right
}

.center,
.aligncenter,
img[align=\\\'middle\\\'] {
    display: block;
    float: none;
    clear: both;
    margin-right: auto;
    margin-left: auto;
    text-align: center
}

.block,
.alignnone {
    display: block;
    clear: both
}

.wp-smiley {
    display: inline
}

input[type=\\\'submit\\\'],
button {
    overflow: visible;
    cursor: pointer;
    -moz-appearance: none;
    -webkit-appearance: none
}

.sidebar {
    padding: $x_single 0 0
}

.sidebar .sidebar_heading,
.sidebar .widget_title {
    font-variant: small-caps;
    margin-bottom: 10px;
    letter-spacing: 1px
}

.sidebar .input_submit {
    font-size: inherit
}

.sidebar .headline,
.sidebar .sidebar_heading,
.sidebar .widget_title {
    font-size: 17px;
    line-height: 24px
}

.widget,
.sidebar .text_box,
.sidebar .thesis_email_form,
.sidebar .query_box {
    margin-bottom: 38px
}

.sidebar .search-form .input_text,
.sidebar .thesis_email_form .input_text {
    width: 100%
}

.sidebar .thesis_email_form .input_text,
.widget li {
    margin-bottom: 10px
}

.sidebar .query_box .post_author,
.sidebar .query_box .post_date {
    color: #888888
}

.sidebar .left,
.sidebar .alignleft,
.sidebar .ad_left {
    margin-right: 19px
}

.sidebar p,
.sidebar ul,
.sidebar ol,
.sidebar blockquote,
.sidebar pre,
.sidebar dl,
.sidebar dd,
.sidebar .left,
.sidebar .alignleft,
.sidebar .ad_left,
.sidebar .right,
.sidebar .alignright,
.sidebar .ad,
.sidebar .center,
.sidebar .aligncenter,
.sidebar .block,
.sidebar .alignnone {
    margin-bottom: 19px
}

.sidebar ul ul,
.sidebar ul ol,
.sidebar ol ul,
.sidebar ol ol,
.sidebar .right,
.sidebar .alignright,
.sidebar .ad,
.sidebar .stack {
    margin-left: 19px
}

a {
    text-decoration: none;
    color: $links
}

p a {
    text-decoration: underline
}

p a:hover {
    text-decoration: none
}

/* ************************************************
*
*  Menu
*
***************************************************/
.menu-box {
    display: block;
    position: relative;
    z-index: 50;
    font-size: 16px;
    width: 100%;
    color: $color3;
    background: $color2;
}

.menu_control {
    display: block;
    position: relative;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 100%;
    padding: 1em 1.618em;
    cursor: pointer;
    letter-spacing: 1px;
    text-transform: uppercase;
    color: $color3;
    background-color: $color2;
}

.menu {
    display: block;
    position: relative;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 100%;
    padding: 0 0 1.618em;
    list-style: none;
    color: $color3;
    background-color: $color2;
}

.menu li a {
    display: block;
    position: relative;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 100%;
    padding: 1em 1.618em;
    cursor: pointer;
    letter-spacing: 1px;
    text-transform: uppercase;
    color: inherit;
    background-color: $color2;
}

/* ===[ Hover Color ]=== */
.menu li a:hover {
    color: $menu_background;
    background-color: $color1
}

/* ===[ Active Color ]=== */
.menu li.current-menu-item > a {
    font-weight: bolder;
    cursor: text;
    color: $menu_background;
    background-color: $color3
}

.menu .sub-menu {
    display: block;
    position: static;
    margin: 0;
    padding-left: 1.618em;
    list-style: none;
    color: inherit;
    background-color: $color2;
}

/* ===[ Menu Borders ]=== */
/* top-menu  */
.menu li > a {
    border: 1px solid $text2
}

/* sub-menu  */
.menu .sub-menu a {
    margin: -1px 0 0;
    border: 1px solid $text2
}

/* active */
.menu li.current-menu-item > a {
    border-bottom-color: $color3
}

.menu {
    display: none
}

.menu-box:hover .menu {
    display: block
}

.mega-menu-visible {
    display: none
}

@media all and (min-width:960px) {
    .menu_control {
        display: none
    }

    .menu {
        display: block;
        padding: 0;
    }

    .menu .sub-menu {
        display: none;
        position: absolute;
        z-index: 110;
        width: $submenu;
        padding: 0;
    }

    .menu .sub-menu .sub-menu {
        top: 0;
        left: $submenu;
        margin: 0 0 0 -1px;
    }

    .menu li {
        float: left;
        position: relative;
        width: auto;
    }

    .menu .sub-menu li {
        clear: both;
        width: 100%;
    }

    .menu li:hover > .sub-menu {
        display: block;
        background-color: $menu_background;
    }

    /* ===[ Menu Borders ]=== */
    /* top-menu */    
    .menu li > a {
        border-right: none;
        border-left: none;
    }

    /* sub-menu */
    /* No change */
    
    /* active */
    /* No change */

    .mega-menu-visible {
        display: block;
        position: absolute;
        z-index: 100;
        width: inherit;
        background-color: $menu_background;
    }

    #mega-menu-item-recents:hover {
        display: block;
        position: absolute;
        z-index: 100;
        width: inherit;
        background-color: $menu_background;
    }
}

.header-box {
    padding: $x_single 0
}

.header-box .social {
    display: block;
    text-align: right
}

.header-box .phone {
    display: block;
    font-size: 2em;
    font-weight: 400;
    margin: 15px 0;
    text-align: right
}

#thesis_header_image {
    width: auto;
    height: auto;
    max-height: 150px
}

#site_title {
    font-size: 2em;
    font-weight: 700;
    line-height: 3em;
    color: $headline_color
}

#site_title a {
    color: $headline_color
}

#site_title a:hover {
    color: $links
}

#site_tagline {
    font-size: 1em;
    color: #888888
}

.grt .headline {
    font-family: Georgia,\\\'Times New Roman\\\',Times,serif;
    font-size: 1.5em;
    margin: 0
}

.grt .drop_cap {
    float: left;
    font-size: 3em;
    margin-right: .15em
}

.grt ul,
.grt ol,
.grt .stack {
    margin-left: $x_single
}

.grt .caption {
    margin-top: -$x_half;
    color: #888888
}

.grt blockquote.right,
.grt blockquote.left {
    font-size: 26px;
    width: 45%;
    margin-bottom: $x_half;
    border: 0;
    padding-left: 0
}

.post_box {
    border-top: 1px dotted $color1;
    padding: $x_single 0 0
}

.post_box .author_description_intro {
    font-weight: 700
}

.post_box .headline_area .headline {
    margin-bottom: 0
}

.post_box .author_description {
    border-top: 1px dotted $color1;
    padding-top: $x_single
}

.post_box .author_description .avatar {
    float: left;
    width: 78px;
    height: 78px;
    margin-right: $x_half;
    margin-left: 0
}

.post_box blockquote {
    margin-left: $x_half;
    border-left: 1px solid $color1;
    padding-left: $x_half;
    color: #888888
}

.grt p,
.grt ul,
.grt ol,
.grt blockquote,
.grt pre,
.grt dl,
.grt dd,
.grt .center,
.grt .block,
.grt .caption,
.post_box .aligncenter,
.post_box .alignnone,
.post_box .post_image,
.post_box .post_image_box,
.post_box .wp-caption,
.post_box .wp-post-image,
.post_box .alert,
.post_box .note,
.headline_area {
    margin-bottom: $x_single
}

.post_box .wp-caption img,
.post_box .post_image_box .post_image,
.post_box .thumb {
    margin-bottom: $x_half
}

.post_box .wp-caption p {
    font-size: $f_aux
}

.grt ul ul,
.grt ul ol,
.grt ol ul,
.grt ol ol,
.wp-caption p,
.post_box .alert p:last-child,
.post_box .note p:last-child,
.post_content blockquote.right p,
.post_content blockquote.left p {
    margin-bottom: 0
}

.grt .right,
.post_box .alignright,
.post_box .ad {
    margin-bottom: $x_single;
    margin-left: $x_single
}

.grt .left,
.post_box .alignleft,
.post_box .ad_left {
    margin-right: $x_single;
    margin-bottom: $x_single
}

.post_box .post_cats,
.post_box .post_tags {
    color: #888888
}

.post_box .headline,
.headline a {
    color: $headline_color
}

.post_box .headline {
    margin-bottom: $x_single
}

.post_box .avatar {
    float: right;
    clear: both;
    width: 61px;
    height: 61px;
    margin-left: $x_half
}

.post_box .alert,
.post_box .note {
    padding: $x_half
}

.post_box .frame,
.post_box .post_image_box,
.post_box .wp-caption {
    border: 1px solid $color1;
    padding: $x_half;
    background-color: $color3
}

.post_box .alert {
    border: 1px solid #e6e68a;
    background-color: #ffff99
}

.post_box .note {
    border: 1px solid $color1;
    background-color: $color3
}

.icon,
.post_box .icon {
    font-weight: 300;
    line-height: 36px;
    margin-bottom: 20px;
    padding-left: 40px;
    background-repeat: no-repeat;
    background-position: left center
}

.post_box code {
    font-family: Consolas,Monaco,Menlo,Courier,Verdana,sans-serif
}

.post_box pre {
    font-family: Consolas,Monaco,Menlo,Courier,Verdana,sans-serif;
    padding: $x_half;
    -moz-tab-size: 4;
    -o-tab-size: 4;
    tab-size: 4;
    background-color: $color3;
}

.post_box ul {
    list-style-type: square
}

.top {
    border-top: 0
}

.headline a:hover {
    color: $links
}

.byline a {
    border-bottom: 1px solid $color1
}

.byline a:hover,
.num_comments {
    color: #111111
}

.byline,
.byline a {
    color: #888888
}

.post_author_intro,
.post_date_intro,
.byline .post_cats_intro {
    font-style: italic
}

.byline .post_edit {
    margin-left: $x_half
}

.byline .post_edit:first-child {
    margin-left: 0
}

.byline a,
.post_author,
.post_date {
    letter-spacing: 1px;
    text-transform: uppercase
}

.post_content li a {
    text-decoration: underline
}

.post_content li a:hover {
    text-decoration: none
}

.wp-caption.aligncenter img {
    margin-right: auto;
    margin-left: auto
}

.num_comments_link {
    display: inline-block;
    margin-bottom: $x_single;
    text-decoration: none;
    color: #888888
}

.num_comments_link:hover {
    text-decoration: underline
}

.bracket,
.num_comments {
    font-size: 1.5em
}

.bracket {
    color: $color1
}

.archive_intro {
    border-width: 0 0 1px;
    border-style: solid;
    border-color: $color1
}

.archive_intro .headline {
    margin-bottom: $x_single
}

.prev_next {
    clear: both;
    border-top: 1px solid $color1;
    padding: $x_half $x_single;
    color: #888888
}

.prev_next .next_posts {
    float: right
}

.previous_posts,
.next_posts {
    display: block;
    font-size: $f_aux;
    letter-spacing: 2px;
    text-transform: uppercase
}

.previous_posts a:hover,
.next_posts a:hover {
    text-decoration: underline
}

#comments {
    margin-top: $x_double
}

.comments_intro {
    margin-bottom: $x_half;
    padding: 0 $x_single;
    color: #888888
}

.comments_closed {
    font-size: $f_aux;
    margin: 0 $x_single $x_single;
    color: #888888
}

.comment_list {
    margin-bottom: $x_double;
    border-top: 1px dotted $color1;
    list-style-type: none
}

.comment {
    border-bottom: 1px dotted $color1;
    padding: $x_single
}

.comment .comment_author {
    font-weight: 700
}

.comment .comment_head {
    margin-bottom: $x_half
}

.comment #commentform {
    margin-top: 0;
    padding-right: 0;
    padding-left: 0
}

.comment .avatar {
    float: right;
    width: $x_double;
    height: $x_double;
    margin-left: $x_half
}

.children .comment {
    margin-top: $x_single;
    border-bottom: 0;
    border-left: 1px solid $color1;
    padding: 0 0 0 $x_single;
    list-style-type: none
}

.children .bypostauthor {
    border-color: $links;
    background-color: transparent
}

.children .comment_head {
    margin-bottom: 0
}

.comment_date {
    font-size: $f_aux;
    margin-left: $x_half;
    color: #888888
}

.comment_date a {
    color: #888888
}

.comment_footer a {
    font-size: $f_aux;
    margin-left: $x_half;
    letter-spacing: 1px;
    text-transform: uppercase;
    color: #888888
}

.comment_footer a:first-child {
    margin-left: 0
}

.comment_head a:hover,
.comment_footer a:hover,
.comment_nav a:hover {
    text-decoration: underline
}

.comment_nav {
    font-size: $f_aux;
    padding: $x_half $x_single;
    letter-spacing: 1px;
    text-transform: uppercase;
    border-style: dotted;
    border-color: $color1
}

.header-box > .container:after,
.columns:after,
.menu:after,
.post_box:after,
.post_content:after,
.author_description:after,
.sidebar:after,
.query_box:after,
.prev_next:after,
.comment_text:after,
.comment_nav:after,
.footer:after {
    display: block;
    visibility: hidden;
    clear: both;
    font-size: 0;
    width: 0;
    height: 0;
    content: \\\'.\\\'
}

.comment_nav_top {
    border-width: 1px 0 0
}

.comment_nav_bottom {
    margin: -$x_double 0 $x_double;
    border-width: 0 0 1px
}

.next_comments {
    float: right
}

.input_text {
    font-family: inherit;
    font-size: inherit;
    font-weight: inherit;
    line-height: 1em;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    border: 1px solid $color1;
    padding: .35em;
    color: #111111;
    background-color: $color3
}

.input_text:focus {
    border-color: $color3;
    background-color: $color3
}

textarea.input_text {
    line-height: $h_text
}

.input_submit {
    overflow: visible;
    font-family: inherit;
    font-size: 1.11em;
    font-weight: 700;
    line-height: 1em;
    border: 3px double $color1;
    padding: .5em;
    cursor: pointer;
    background: $color3 url(images/bg-button.png) repeat-x
}

#commentform {
    margin: $x_double 0;
    padding: 0 $x_single
}

#commentform textarea.input_text {
    width: 100%
}

#commentform .input_text {
    width: 50%
}

#commentform label {
    display: block
}

#commentform p {
    margin-bottom: $x_half
}

#commentform p .required {
    color: #dd0000
}

.comment_form_title {
    font-size: 1.11em;
    margin: 0 -$x_single;
    border-bottom: 1px dotted $color1;
    padding: 0 $x_single $x_half;
    color: $headline_color
}

.comment_moderated {
    font-weight: 700
}

#cancel-comment-reply-link {
    float: right;
    font-size: $f_aux;
    letter-spacing: 1px;
    text-transform: uppercase;
    color: $links
}

#cancel-comment-reply-link:hover {
    text-decoration: underline
}

.login_alert {
    font-weight: 700;
    border: 1px solid $color1;
    background-color: $color3
}

.sidebar {
    font-size: $f_aux
}

.widget ul {
    list-style-type: none
}

.widget li a:hover {
    text-decoration: underline
}

.sidebar .post_content,
.widget li ul,
.widget li ol {
    margin-top: 10px
}

.footer-box {
    font-size: $f_aux;
    border-top: 3px double $color1;
    color: #888888
}

.footer-box a {
    color: #888888
}

.footer-box a:hover {
    color: #111111
}

.icon-1 {
    background-image: url(images/icon-1.png)
}

.icon-2 {
    background-image: url(images/icon-2.png)
}

.icon-3 {
    background-image: url(images/icon-3.png)
}

.icon-4 {
    background-image: url(images/icon-4.png)
}

.icon-5 {
    background-image: url(images/icon-5.png)
}

.icon-6 {
    background-image: url(images/icon-6.png)
}

.icon-7 {
    background-image: url(images/icon-7.png)
}

.icon-8 {
    background-image: url(images/icon-8.png)
}

.icon-9 {
    background-image: url(images/icon-9.png)
}

.icon-10 {
    background-image: url(images/icon-10.png)
}

.ss li a:active {
    -webkit-box-shadow: inset 0 0 10px rgba(0,0,0,.3),inset 0 0 10px rgba(0,0,0,.3);
    -moz-box-shadow: inset 0 0 10px rgba(0,0,0,.3),inset 0 0 10px rgba(0,0,0,.3);
    box-shadow: inset 0 0 10px rgba(0,0,0,.3),inset 0 0 10px rgba(0,0,0,.3)
}

.ss li a,
.ss li a:hover,
.ssm li a,
.ssm li a:hover {
    padding: 0;
    background: none;
    text-shadow: none
}

.ssm li a:active {
    -webkit-box-shadow: inset 0 0 10px rgba(0,0,0,.3),inset 0 0 10px rgba(0,0,0,.3);
    -moz-box-shadow: inset 0 0 10px rgba(0,0,0,.3),inset 0 0 10px rgba(0,0,0,.3);
    box-shadow: inset 0 0 10px rgba(0,0,0,.3),inset 0 0 10px rgba(0,0,0,.3)
}

.ss {
    width: auto;
    margin: 0;
    padding: 0;
    text-align: left
}

.ss li.customicon1 {
    background-position: 0 0
}

.ss li.customicon1:hover {
    background-position: 0 -40px
}

.ss li.stumbleupon {
    background-position: 0 -1360px
}

.ss li.stumbleupon:hover {
    background-position: 0 -1400px
}

.ss li.customicon2 {
    background-position: 0 0
}

.ss li.customicon2:hover {
    background-position: 0 -40px
}

.ss li.googleplus {
    background-position: 0 -400px
}

.ss li.googleplus:hover {
    background-position: 0 -440px
}

.ss li.deviantart {
    background-position: 0 -2960px
}

.ss li.deviantart:hover {
    background-position: 0 -3000px
}

.ss li.foursquare {
    background-position: 0 -2320px
}

.ss li.foursquare:hover {
    background-position: 0 -2360px
}

.ss li.pinterest {
    background-position: 0 -1040px
}

.ss li.pinterest:hover {
    background-position: 0 -1080px
}

.ss li.wordpress {
    background-position: 0 -1680px
}

.ss li.wordpress:hover {
    background-position: 0 -1720px
}

.ss li.instagram {
    background-position: 0 -2240px
}

.ss li.instagram:hover {
    background-position: 0 -2280px
}

.ss li.linkedin {
    background-position: 0 -720px
}

.ss li.linkedin:hover {
    background-position: 0 -760px
}

.ss li.bandcamp {
    background-position: 0 -2800px
}

.ss li.bandcamp:hover {
    background-position: 0 -2840px
}

.ss li.dribbble {
    background-position: 0 -80px
}

.ss li.dribbble:hover {
    background-position: 0 -120px
}

.ss li.facebook {
    background-position: 0 -160px
}

.ss li.facebook:hover {
    background-position: 0 -200px
}

.ss li.myspace {
    background-position: 0 -800px
}

.ss li.myspace:hover {
    background-position: 0 -840px
}

.ss li.aboutme {
    background-position: 0 -2720px
}

.ss li.aboutme:hover {
    background-position: 0 -2760px
}

.ss li.youtube {
    background-position: 0 -1840px
}

.ss li.youtube:hover {
    background-position: 0 -1880px
}

.ss li.twitter {
    background-position: 0 -1520px
}

.ss li.twitter:hover {
    background-position: 0 -1560px
}

.ss li.behance {
    background-position: 0 -2000px
}

.ss li.behance:hover {
    background-position: 0 -2040px
}

.ss li.github {
    background-position: 0 -1920px
}

.ss li.github:hover {
    background-position: 0 -1960px
}

.ss li.lastfm {
    background-position: 0 -640px
}

.ss li.lastfm:hover {
    background-position: 0 -680px
}

.ss li.zerply {
    background-position: 0 -2400px
}

.ss li.zerply:hover {
    background-position: 0 -2440px
}

.ss li.forrst {
    background-position: 0 -320px
}

.ss li.forrst:hover {
    background-position: 0 -360px
}

.ss li.icloud {
    background-position: 0 -560px
}

.ss li.icloud:hover {
    background-position: 0 -600px
}

.ss li.picasa {
    background-position: 0 -960px
}

.ss li.picasa:hover {
    background-position: 0 -1000px
}

.ss li.paypal {
    background-position: 0 -880px
}

.ss li.paypal:hover {
    background-position: 0 -920px
}

.ss li.reddit {
    background-position: 0 -1120px
}

.ss li.reddit:hover {
    background-position: 0 -1160px
}

.ss li.flickr {
    background-position: 0 -240px
}

.ss li.flickr:hover {
    background-position: 0 -280px
}

.ss li.tumblr {
    background-position: 0 -1440px
}

.ss li.tumblr:hover {
    background-position: 0 -1480px
}

.ss li.yahoo {
    background-position: 0 -1760px
}

.ss li.yahoo:hover {
    background-position: 0 -1800px
}

.ss li.skype {
    background-position: 0 -1280px
}

.ss li.skype:hover {
    background-position: 0 -1320px
}

.ss li.html5 {
    background-position: 0 -480px
}

.ss li.html5:hover {
    background-position: 0 -520px
}

.ss li.share {
    background-position: 0 -2560px
}

.ss li.share:hover {
    background-position: 0 -2600px
}

.ss li.vimeo {
    background-position: 0 -1600px
}

.ss li.vimeo:hover {
    background-position: 0 -1640px
}

.ss li.digg {
    background-position: 0 0
}

.ss li.digg:hover {
    background-position: 0 -40px
}

.ss li.xing {
    background-position: 0 -3040px
}

.ss li.xing:hover {
    background-position: 0 -3080px
}

.ss li.star {
    background-position: 0 -2480px
}

.ss li.star:hover {
    background-position: 0 -2520px
}

.ss li.mail {
    background-position: 0 -2160px
}

.ss li.mail:hover {
    background-position: 0 -2200px
}

.ss li.viki {
    background-position: 0 -2880px
}

.ss li.viki:hover {
    background-position: 0 -2920px
}

.ss li.yelp {
    background-position: 0 -2080px
}

.ss li.yelp:hover {
    background-position: 0 -2120px
}

.ss li.doc {
    background-position: 0 -2640px
}

.ss li.doc:hover {
    background-position: 0 -2680px
}

.ss li.rss {
    background-position: 0 -1200px
}

.ss li.rss:hover {
    background-position: 0 -1240px
}

.ss li {
    display: inline-block;
    width: 40px;
    height: 40px;
    margin: 0;
    border-right: 1px solid #050505;
    border-right: 1px solid rgba(0,0,0,.2);
    border-bottom: none;
    padding: 0;
    -webkit-transition: all .3s ease;
    -moz-transition: all .3s ease;
    -ms-transition: all .3s ease;
    -o-transition: all .3s ease;
    transition: all .3s ease
}

.ss li {
    background: url(images/socialsprites.png) no-repeat
}

.ss li:first-child {
    border-left: 1px solid;
    border-left-color: rgb(5,5,5);
    border-left-color: rgba(0,0,0,.2)
}

.ss li:hover {
    -webkit-transition: all .3s ease;
    -moz-transition: all .3s ease;
    -ms-transition: all .3s ease;
    -o-transition: all .3s ease;
    transition: all .3s ease
}

.ss li a {
    display: block;
    width: 40px;
    height: 40px
}

.ss,
.ssm {
    display: inline-block;
    font-size: 0;
    background-color: #eeeeee
}

.ssm li.customicon1 {
    background-position: 0 0
}

.ssm li.customicon1:hover {
    background-position: 0 -40px
}

.ssm li.customicon2 {
    background-position: 0 0
}

.ssm li.customicon2:hover {
    background-position: 0 -40px
}

.ssm li.stumbleupon {
    background-position: 0 -1020px
}

.ssm li.stumbleupon:hover {
    background-position: 0 -1050px
}

.ssm li.foursquare {
    background-position: 0 -1740px
}

.ssm li.foursquare:hover {
    background-position: 0 -1770px
}

.ssm li.googleplus {
    background-position: 0 -300px
}

.ssm li.googleplus:hover {
    background-position: 0 -330px
}

.ssm li.deviantart {
    background-position: 0 -2220px
}

.ssm li.deviantart:hover {
    background-position: 0 -2250px
}

.ssm li.instagram {
    background-position: 0 -1680px
}

.ssm li.instagram:hover {
    background-position: 0 -1710px
}

.ssm li.pinterest {
    background-position: 0 -780px
}

.ssm li.pinterest:hover {
    background-position: 0 -810px
}

.ssm li.wordpress {
    background-position: 0 -1260px
}

.ssm li.wordpress:hover {
    background-position: 0 -1290px
}

.ssm li.facebook {
    background-position: 0 -120px
}

.ssm li.facebook:hover {
    background-position: 0 -150px
}

.ssm li.bandcamp {
    background-position: 0 -2100px
}

.ssm li.bandcamp:hover {
    background-position: 0 -2130px
}

.ssm li.linkedin {
    background-position: 0 -540px
}

.ssm li.linkedin:hover {
    background-position: 0 -570px
}

.ssm li.dribbble {
    background-position: 0 -60px
}

.ssm li.dribbble:hover {
    background-position: 0 -90px
}

.ssm li.behance {
    background-position: 0 -1500px
}

.ssm li.behance:hover {
    background-position: 0 -1530px
}

.ssm li.youtube {
    background-position: 0 -1380px
}

.ssm li.youtube:hover {
    background-position: 0 -1410px
}

.ssm li.myspace {
    background-position: 0 -600px
}

.ssm li.myspace:hover {
    background-position: 0 -630px
}

.ssm li.twitter {
    background-position: 0 -1140px
}

.ssm li.twitter:hover {
    background-position: 0 -1170px
}

.ssm li.aboutme {
    background-position: 0 -2040px
}

.ssm li.aboutme:hover {
    background-position: 0 -2070px
}

.ssm li.reddit {
    background-position: 0 -840px
}

.ssm li.reddit:hover {
    background-position: 0 -870px
}

.ssm li.paypal {
    background-position: 0 -660px
}

.ssm li.paypal:hover {
    background-position: 0 -690px
}

.ssm li.zerply {
    background-position: 0 -1800px
}

.ssm li.zerply:hover {
    background-position: 0 -1830px
}

.ssm li.icloud {
    background-position: 0 -420px
}

.ssm li.icloud:hover {
    background-position: 0 -450px
}

.ssm li.picasa {
    background-position: 0 -720px
}

.ssm li.picasa:hover {
    background-position: 0 -750px
}

.ssm li.forrst {
    background-position: 0 -240px
}

.ssm li.forrst:hover {
    background-position: 0 -270px
}

.ssm li.flickr {
    background-position: 0 -180px
}

.ssm li.flickr:hover {
    background-position: 0 -210px
}

.ssm li.lastfm {
    background-position: 0 -480px
}

.ssm li.lastfm:hover {
    background-position: 0 -510px
}

.ssm li.tumblr {
    background-position: 0 -1080px
}

.ssm li.tumblr:hover {
    background-position: 0 -1110px
}

.ssm li.github {
    background-position: 0 -1440px
}

.ssm li.github:hover {
    background-position: 0 -1470px
}

.ssm li.vimeo {
    background-position: 0 -1200px
}

.ssm li.vimeo:hover {
    background-position: 0 -1230px
}

.ssm li.yahoo {
    background-position: 0 -1320px
}

.ssm li.yahoo:hover {
    background-position: 0 -1350px
}

.ssm li.share {
    background-position: 0 -1920px
}

.ssm li.share:hover {
    background-position: 0 -1950px
}

.ssm li.skype {
    background-position: 0 -960px
}

.ssm li.skype:hover {
    background-position: 0 -990px
}

.ssm li.html5 {
    background-position: 0 -360px
}

.ssm li.html5:hover {
    background-position: 0 -390px
}

.ssm li.digg {
    background-position: 0 0
}

.ssm li.digg:hover {
    background-position: 0 -30px
}

.ssm li.viki {
    background-position: 0 -2160px
}

.ssm li.viki:hover {
    background-position: 0 -2190px
}

.ssm li.yelp {
    background-position: 0 -1560px
}

.ssm li.yelp:hover {
    background-position: 0 -1590px
}

.ssm li.xing {
    background-position: 0 -2280px
}

.ssm li.xing:hover {
    background-position: 0 -2310px
}

.ssm li.mail {
    background-position: 0 -1620px
}

.ssm li.mail:hover {
    background-position: 0 -1650px
}

.ssm li.star {
    background-position: 0 -1860px
}

.ssm li.star:hover {
    background-position: 0 -1890px
}

.ssm li.doc {
    background-position: 0 -1980px
}

.ssm li.doc:hover {
    background-position: 0 -2010px
}

.ssm li.rss {
    background-position: 0 -900px
}

.ssm li.rss:hover {
    background-position: 0 -930px
}

.ssm li {
    display: inline-block;
    width: 30px;
    height: 30px;
    margin: 0;
    border-right: 1px solid;
    border-bottom: none;
    padding: 0;
    -webkit-transition: all .3s ease;
    -moz-transition: all .3s ease;
    -ms-transition: all .3s ease;
    -o-transition: all .3s ease;
    transition: all .3s ease;
    border-right-color: #050505;
    border-right-color: rgba(0,0,0,.2)
}

.ss.ss_white,
.ssm.ssm_white {
    background-color: $menu_background
}

.ssm li {
    background: url(images/socialsprites_mini.png) no-repeat
}

.ssm li:first-child {
    border-left: 1px solid #050505;
    border-left: 1px solid rgba(0,0,0,.2)
}

.ssm.ssm_white li {
    background: url(images/socialsprites_mini_wht.png) no-repeat
}

.ss.ss_white li {
    background: url(images/socialsprites_wht.png) no-repeat
}

.ssm li:hover {
    -webkit-transition: all .3s ease;
    -moz-transition: all .3s ease;
    -ms-transition: all .3s ease;
    -o-transition: all .3s ease;
    transition: all .3s ease
}

.ssm li a {
    display: block;
    width: 30px;
    height: 30px
}

.socialbar_mini {
    z-index: 999;
    width: 100%;
    height: 30px;
    padding: 0
}

.ssm {
    font-size: 0;
    width: auto;
    margin: 0;
    padding: 0;
    text-align: left
}

.socialbar {
    z-index: 999;
    font-size: 0;
    width: 100%;
    min-height: 40px;
    padding: 0;
    background: #ffffff
}

.socialbar:after {
    clear: left;
    content: \\\' \\\'
}

.socialbar_transparent {
    z-index: 999;
    font-size: 0;
    width: 100%;
    min-height: 40px;
    padding: 0
}

.socialbar_transparent:after {
    clear: left;
    content: \\\' \\\'
}

.borderless li {
    border: none
}

.borderless li:first-child {
    border: none
}

ul.borderless li {
    border: none
}

ul.borderless li:first-child {
    border: none
}

.sssquare {
    -webkit-border-radius: 0;
    -moz-border-radius: 0;
    border-radius: 0
}

.sscircle li {
    margin-right: 5px;
    margin-bottom: 5px;
    -webkit-transition: all .3s ease;
    -moz-transition: all .3s ease;
    -ms-transition: all .3s ease;
    -o-transition: all .3s ease;
    transition: all .3s ease;
    -webkit-border-radius: 100%;
    -moz-border-radius: 100%;
    border-radius: 100%;
    background-color: #ffffff;
    -webkit-box-shadow: 0 2px rgba(0,0,0,.3);
    -moz-box-shadow: 0 2px rgba(0,0,0,.3);
    box-shadow: 0 2px rgba(0,0,0,.3)
}

.sscircle li:hover,
.sscircle li a:active {
    -webkit-transition: all .3s ease;
    -moz-transition: all .3s ease;
    -ms-transition: all .3s ease;
    -o-transition: all .3s ease;
    transition: all .3s ease
}

.container,
.container.fixed {
    width: 320px;
    max-width: 320px
}

.grid {
    position: relative;
    font-size: 0
}

.grid-item {
    display: inline-block;
    font-size: $f_text;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 97.92%;
    margin-right: 1.04%;
    margin-left: 1.04%;
    vertical-align: top
}

.grid-1 {
    width: 6.25%
}

.grid-2 {
    width: 14.59%
}

.grid-3 {
    width: 22.92%
}

.grid-4 {
    width: 31.25%
}

.grid-5 {
    width: 39.59%
}

.grid-6 {
    width: 47.92%
}

.grid-7 {
    width: 56.25%
}

.grid-8 {
    width: 64.59%
}

.grid-9 {
    width: 72.92%
}

.grid-10 {
    width: 81.25%
}

.grid-11 {
    width: 89.59%
}

.grid-12 {
    width: 97.92%
}

@media all and (min-width:320px) {
    .container {
        width: auto;
        max-width: 479px;
    }

    .container.fixed {
        width: 310px
    }

    .g320-1 {
        width: 6.25%
    }

    .g320-2 {
        width: 14.59%
    }

    .g320-3 {
        width: 22.92%
    }

    .g320-4 {
        width: 31.25%
    }

    .g320-5 {
        width: 39.59%
    }

    .g320-6 {
        width: 47.92%
    }

    .g320-7 {
        width: 56.25%
    }

    .g320-8 {
        width: 64.59%
    }

    .g320-9 {
        width: 72.92%
    }

    .g320-10 {
        width: 81.25%
    }

    .g320-11 {
        width: 89.59%
    }

    .g320-12 {
        width: 97.92%
    }
}

@media all and (min-width:480px) {
    .container {
        width: auto;
        max-width: 719px;
    }

    .container.fixed {
        width: 470px
    }

    .g480-1 {
        width: 6.25%
    }

    .g480-2 {
        width: 14.59%
    }

    .g480-3 {
        width: 22.92%
    }

    .g480-4 {
        width: 31.25%
    }

    .g480-5 {
        width: 39.59%
    }

    .g480-6 {
        width: 47.92%
    }

    .g480-7 {
        width: 56.25%
    }

    .g480-8 {
        width: 64.59%
    }

    .g480-9 {
        width: 72.92%
    }

    .g480-10 {
        width: 81.25%
    }

    .g480-11 {
        width: 89.59%
    }

    .g480-12 {
        width: 97.92%
    }
}

@media all and (min-width:720px) {
    .container {
        width: auto;
        max-width: 959px;
    }

    .container.fixed {
        width: 720px
    }

    .g720-1 {
        width: 6.25%
    }

    .g720-2 {
        width: 14.59%
    }

    .g720-3 {
        width: 22.92%
    }

    .g720-4 {
        width: 31.25%
    }

    .g720-5 {
        width: 39.59%
    }

    .g720-6 {
        width: 47.92%
    }

    .g720-7 {
        width: 56.25%
    }

    .g720-8 {
        width: 64.59%
    }

    .g720-9 {
        width: 72.92%
    }

    .g720-10 {
        width: 81.25%
    }

    .g720-11 {
        width: 89.59%
    }

    .g720-12 {
        width: 97.92%
    }
}

@media all and (min-width:960px) {
    .container,
    .container.fixed {
        width: 960px;
        max-width: 960px;
        margin-right: auto;
        margin-left: auto;
    }

    .g960-1 {
        width: 6.25%
    }

    .g960-2 {
        width: 14.59%
    }

    .g960-3 {
        width: 22.92%
    }

    .g960-4 {
        width: 31.25%
    }

    .g960-5 {
        width: 39.59%
    }

    .g960-6 {
        width: 47.92%
    }

    .g960-7 {
        width: 56.25%
    }

    .g960-8 {
        width: 64.59%
    }

    .g960-9 {
        width: 72.92%
    }

    .g960-10 {
        width: 81.25%
    }

    .g960-11 {
        width: 89.59%
    }

    .g960-12 {
        width: 97.92%
    }
}

.row {
    clear: both;
    width: 100%
}

.header-box {
    position: relative;
    width: 100%;
    color: inherit;
    background: none
}

.intro {
    position: relative;
    width: 100%
}

.intro-box {
    position: relative;
    width: 100%;
    height: 400px;
    background: url(images/intro-bg-1.jpg) no-repeat -261px -111px
}

.attribution {
    float: right;
    clear: both
}

.c2g_teaser_item {
    display: inline-block;
    margin: 5px 10px 0 0;
    text-align: left;
}

.c2g_teaser_item .c2g_teaser_thumbnail {
    display: inline-block;
    line-height: $thumb_height;
    width: $thumb_width;
    background: #555555;
}

.c2g_teaser_item .c2g_teaser_thumbnail img {
    display: inline-block;
    width: 100%;
    height: auto;
    margin: 0;
    vertical-align: middle;
}

.c2g_teaser_item .c2g_teaser_excerpt {
    display: inline-block;
    width: 71%;
    margin: 2%
}

.c2g_teaser_item .c2g_teaser_title {
    display: block;
    width: 100%
}

@media all and (min-width:720px) {
    .c2g_teaser .g720-4 {
        text-align: center
    }

    .g720-4 .c2g_teaser_title {
        width: 100%
    }

    .g720-4 .c2g_teaser_thumbnail {
        width: $thumb_width;
        height: $thumb_height;
    }

    .g720-4 .c2g_teaser_excerpt {
        width: 100%
    }
}

#mc_embed_signup {
    line-height: $h_aux
}

#mc_embed_signup .email {
    position: relative;
    box-sizing: border-box;
    width: 100%;
    height: $h_aux;
    padding: 3px
}

#mc-embedded-subscribe {
    line-height: $h_aux;
    padding: 0 10px
}

.template-front .container .columns .content .post_box {
    margin-bottom: 20px
}

.featured_image {
    display: inline-block;
    width: 300px;
    height: 300px;
    margin-right: 1.5em;
    margin-bottom: 1.5em
}

.featured_image .child-nav ul li.separator {
    display: none
}

.featured_image .child-nav-container {
    position: absolute;
    top: 0;
    left: -4px
}

.featured_image .child-nav ul li {
    display: block;
    clear: both;
    width: 100%;
    margin-bottom: 1.4px;
    border-bottom: none;
    padding: 0 15px
}

.featured_image .child-nav ul li a {
    display: block;
    position: relative;
    left: -15px;
    font-weight: 100;
    padding: 3px 0 3px 21px;
    text-transform: uppercase;
    color: #fefefe;
    background: url(images/child_nav_arrow.png) no-repeat scroll 0 -4px;
    background-color: #e59739;
    background-color: rgba(229,151,57,.75)
}

.featured_image .child-nav ul li a:hover {
    color: #cccccc
}

.featured_image img {
    width: auto;
    height: auto;
    background-color: #dadada;
    -webkit-box-shadow: 0 4px 9px 3px rgba(0,0,0,.4);
    -moz-box-shadow: 0 4px 9px 3px rgba(0,0,0,.4);
    box-shadow: 0 4px 9px 3px rgba(0,0,0,.4);
    -ms-filter: \\\'progid:DXImageTransform.Microsoft.Shadow(color=#aaaaaa,direction=0,strength=5),progid:DXImageTransform.Microsoft.Shadow(color=#aaaaaa,direction=45,strength=5),progid:DXImageTransform.Microsoft.Shadow(color=#aaaaaa,direction=90,strength=5),progid:DXImageTransform.Microsoft.Shadow(color=#aaaaaa,direction=135,strength=5),progid:DXImageTransform.Microsoft.Shadow(color=#aaaaaa,direction=180,strength=10),progid:DXImageTransform.Microsoft.Shadow(color=#aaaaaa,direction=225,strength=5),progid:DXImageTransform.Microsoft.Shadow(color=#aaaaaa,direction=270,strength=5),progid:DXImageTransform.Microsoft.Shadow(color=#aaaaaa,direction=315,strength=5)\\\';
    -ms-zoom: 1
}

#breadcrumbs,
.child-nav ul {
    display: block;
    clear: both;
    overflow: hidden;
    position: relative;
    font-size: 14px;
    margin: 10px 0;
    list-style: none outside none
}

#breadcrumbs .separator,
.child-nav .separator {
    font-size: 20px;
    font-weight: 700;
    color: #999999
}

#breadcrumbs li,
.child-nav li {
    float: left;
    margin-right: 2px
}

.child-nav .child-thumb {
    width: $thumb_width;
    height: $thumb_height;
    margin: 7px;
    border: 1px solid #d1d1d1;
    background: none repeat scroll 0 0;
    background-color: #eaeaea;
    background-color: rgba(0,0,0,.1);
    box-shadow: 1px 1px 3px 0 #050505;
    box-shadow: 1px 1px 3px 0 rgba(0,0,0,.25)
}

.child-thumb a {
    display: block;
    line-height: $thumb_height;
    text-align: center
}

.child-thumb a img {
    display: inline;
    vertical-align: middle
}

.featured_image {
    position: relative
}

.page-not-found {
    font-size: 24px;
    line-height: 36px;
    margin: 20px auto;
    text-align: center
}

.page-not-found h2 {
    font-size: 36px;
    line-height: 60px
}

.bottom {
    vertical-align: bottom
}

.box-hr {
    display: block;
    clear: both;
    width: 100%;
    height: 2px;
    background: url(images/bg-sectionsep.png) no-repeat top
}

.post_nav {
    display: inline-block
}

.post_nav .prev_next {
    display: block;
    width: 100%;
    border: none;
    padding: 0
}

.post_nav .prev_next .previous_post,
.post_nav .prev_next .next_post {
    display: inline-block
}

.post_nav .prev_next a {
    padding-right: 5px;
    padding-left: 5px;
    color: #eeeeee
}

.post_nav .prev_next p {
    margin-bottom: 0
}

#header #site_title a,
#header #site_tagline {
    display: none
}',
  'boxes' => 
  array (
    'thesis_html_container' => 
    array (
      'thesis_html_container_1348009564' => 
      array (
        'id' => 'logo',
        'class' => 'logo',
        '_id' => 'header',
        '_name' => 'Logo',
      ),
      'thesis_html_container_1348009571' => 
      array (
        'class' => 'grid container',
        '_admin' => 
        array (
          'open' => true,
        ),
        '_id' => 'columns',
        '_name' => 'Grid Container',
      ),
      'thesis_html_container_1348009575' => 
      array (
        'class' => 'footer-box row',
        '_id' => 'footer',
        '_name' => 'Footer Box',
      ),
      'thesis_html_container_1348010954' => 
      array (
        'class' => 'grid-item grid-12',
        '_admin' => 
        array (
          'open' => true,
        ),
        '_id' => 'content',
        '_name' => 'Grid Item - 12',
      ),
      'thesis_html_container_1348010964' => 
      array (
        'class' => 'sidebar',
        '_id' => 'sidebar',
        '_name' => 'Sidebar',
      ),
      'thesis_html_container_1348093642' => 
      array (
        'class' => 'container',
        '_admin' => 
        array (
          'open' => true,
        ),
        '_id' => 'container',
        '_name' => 'Container',
      ),
      'thesis_html_container_1348165494' => 
      array (
        'class' => 'byline small',
        '_name' => 'Byline',
      ),
      'thesis_html_container_1348608649' => 
      array (
        'class' => 'archive_intro post_box grt top',
        '_name' => 'Archive Intro',
      ),
      'thesis_html_container_1348701154' => 
      array (
        'class' => 'prev_next',
        '_id' => 'prev_next',
        '_name' => 'Prev/Next',
      ),
      'thesis_html_container_1348841704' => 
      array (
        'class' => 'comment_head',
        '_name' => 'Comment Head',
      ),
      'thesis_html_container_1348886177' => 
      array (
        'class' => 'headline_area',
        '_name' => 'Headline Area',
      ),
      'thesis_html_container_1365640887' => 
      array (
        'id' => 'comments',
        '_id' => 'post_comments',
        '_name' => 'Post Comments',
      ),
      'thesis_html_container_1365640949' => 
      array (
        'id' => 'comments',
        '_id' => 'page_comments',
        '_name' => 'Page Comments',
      ),
      'thesis_html_container_1366209424' => 
      array (
        'class' => 'comment_footer',
        '_name' => 'Comment Footer',
      ),
      'thesis_html_container_1421106828' => 
      array (
        'class' => 'container mega-menu',
        '_name' => 'Mega Menu Box',
      ),
      'thesis_html_container_1421159249' => 
      array (
        'class' => 'intro-box',
        '_name' => 'Intro Box',
      ),
      'thesis_html_container_1421159324' => 
      array (
        'class' => 'menu-box mega-menu row',
        '_name' => 'Menu Box',
      ),
      'thesis_html_container_1421159875' => 
      array (
        'class' => 'header-box row',
        '_admin' => 
        array (
          'open' => true,
        ),
        '_name' => 'Header Box',
      ),
      'thesis_html_container_1421160415' => 
      array (
        'class' => 'logo-box',
        '_name' => 'Logo Box',
      ),
      'thesis_html_container_1421160524' => 
      array (
        'class' => 'container',
        '_name' => 'Container',
      ),
      'thesis_html_container_1421160996' => 
      array (
        'class' => 'grid-item grid-12',
        '_name' => 'Grid Item - 12',
      ),
      'thesis_html_container_1422909884' => 
      array (
        'class' => 'container',
        '_name' => 'Container',
      ),
      'thesis_html_container_1422910782' => 
      array (
        'class' => 'container',
        '_name' => 'Container',
      ),
      'thesis_html_container_1423685283' => 
      array (
        'class' => 'feature-box row',
        '_name' => 'Feature Box',
      ),
      'thesis_html_container_1424230803' => 
      array (
        'id' => 'mega-menu-item-recents',
        'class' => 'hidden',
        '_name' => 'Mega Menu - Recents Box',
      ),
      'thesis_html_container_1424271399' => 
      array (
        'class' => 'phone',
        '_name' => 'Phone Box',
      ),
      'thesis_html_container_1424271461' => 
      array (
        'class' => 'social',
        '_name' => 'Social Menu Box',
      ),
      'thesis_html_container_1424272580' => 
      array (
        'class' => 'grid container',
        '_admin' => 
        array (
          'open' => true,
        ),
        '_name' => 'Grid Container',
      ),
      'thesis_html_container_1424274879' => 
      array (
        'class' => 'grid-item g720-4 bottom',
        '_name' => 'Grid Item - 12>4',
      ),
      'thesis_html_container_1424274935' => 
      array (
        'class' => 'grid-item g720-8 bottom',
        '_name' => 'Grid Item - 12>8',
      ),
      'thesis_html_container_1424374465' => 
      array (
        'class' => 'featured_image',
        '_name' => 'Featured Image Box',
      ),
      'thesis_html_container_1424916329' => 
      array (
        'class' => 'content-box row',
        '_name' => 'Content Box',
      ),
      'thesis_html_container_1425055684' => 
      array (
        'id' => 'section-3',
        'class' => 'section-box grid-item grid-12',
        '_name' => 'Section 3 Box',
      ),
      'thesis_html_container_1425055793' => 
      array (
        'id' => 'section-4',
        'class' => 'section-box grid-item grid-12',
        '_name' => 'Section 4 Box',
      ),
      'thesis_html_container_1425074763' => 
      array (
        'class' => 'grid-item g480-6 g720-3',
        '_name' => 'Taxonomy Filter Box (12>6>3)',
      ),
      'thesis_html_container_1425074976' => 
      array (
        'class' => 'grid-item g480-6 g720-9',
        '_name' => 'Filtered Results Box (12>6>9)',
      ),
      'thesis_html_container_1425170103' => 
      array (
        'class' => 'post_nav',
        '_name' => 'Post Navigation',
      ),
      'thesis_html_container_1425312086' => 
      array (
        'class' => 'container',
        '_name' => 'Copyright Box',
      ),
    ),
    'thesis_wp_nav_menu' => 
    array (
      'thesis_wp_nav_menu_1348009742' => 
      array (
        'menu' => '2',
        'control' => 
        array (
          'yes' => true,
        ),
        'control_text' => 'Menu',
        '_name' => 'Menu',
      ),
      'thesis_wp_nav_menu_1424271486' => 
      array (
        'menu' => '3',
        'menu_class' => 'ss',
        '_name' => 'Social Menu',
      ),
      'thesis_wp_nav_menu_1426459692' => 
      array (
        'menu_class' => 'menu admin-menu',
        '_name' => 'Admin Links',
      ),
      'thesis_wp_nav_menu_1426459748' => 
      array (
        'menu_class' => 'menu legal-menu',
        '_name' => 'Legal Links',
      ),
    ),
    'thesis_post_box' => 
    array (
      'thesis_post_box_1348010947' => 
      array (
        'class' => 'grt',
        'schema' => 'article',
        '_admin' => 
        array (
          'open' => true,
        ),
        '_name' => 'Home Post Box',
      ),
      'thesis_post_box_1348607689' => 
      array (
        'class' => 'grt',
        'schema' => 'article',
        '_name' => 'Post/Page Post Box',
      ),
    ),
    'thesis_post_headline' => 
    array (
      'thesis_post_box_1348010947_thesis_post_headline' => 
      array (
        'html' => 'h2',
        'link' => 
        array (
          'on' => true,
        ),
        '_parent' => 'thesis_post_box_1348010947',
      ),
      'thesis_post_list_1425075570_thesis_post_headline' => 
      array (
        'link' => 
        array (
          'on' => true,
        ),
        '_parent' => 'thesis_post_list_1425075570',
      ),
    ),
    'thesis_wp_widgets' => 
    array (
      'thesis_wp_widgets_1348079687' => 
      array (
        '_id' => 'sidebar',
        '_name' => 'Sidebar Widgets',
      ),
      'thesis_wp_widgets_1391881268' => 
      array (
        'class' => 'intro-widget',
        '_name' => 'Intro Widget',
      ),
      'thesis_wp_widgets_1391880855' => 
      array (
        'class' => 'grid',
        '_name' => 'Footer Widgets',
      ),
      'thesis_wp_widgets_1396619969' => 
      array (
        '_name' => 'Feature Widgets',
      ),
      'thesis_wp_widgets_1424183841' => 
      array (
        '_name' => 'Mega Menu - Recents',
      ),
      'thesis_wp_widgets_1424716390' => 
      array (
        'class' => 'grid-item page-not-found',
        'title_tag' => 'h2',
        '_name' => '404 Message',
      ),
      'thesis_wp_widgets_1425056136' => 
      array (
        'class' => 'section grid',
        'title_tag' => 'h2',
        '_name' => 'Section 3',
      ),
      'thesis_wp_widgets_1425056165' => 
      array (
        'class' => 'section grid',
        'title_tag' => 'h2',
        '_name' => 'Section 4',
      ),
      'thesis_wp_widgets_1425074917' => 
      array (
        'class' => 'product-filter',
        '_name' => 'Taxonomy Filter',
      ),
      'thesis_wp_widgets_1426687710' => 
      array (
        '_name' => 'Intro Box 2',
      ),
    ),
    'thesis_post_author' => 
    array (
      'thesis_post_box_1348010947_thesis_post_author' => 
      array (
        'intro' => 'by',
        '_id' => 'loop',
        '_parent' => 'thesis_post_box_1348010947',
      ),
      'thesis_post_box_1348607689_thesis_post_author' => 
      array (
        'intro' => 'by',
        '_id' => 'loop',
        '_parent' => 'thesis_post_box_1348607689',
      ),
    ),
    'thesis_post_date' => 
    array (
      'thesis_post_box_1348010947_thesis_post_date' => 
      array (
        'intro' => 'on',
        '_id' => 'loop',
        '_parent' => 'thesis_post_box_1348010947',
      ),
      'thesis_post_box_1348607689_thesis_post_date' => 
      array (
        'intro' => 'on',
        '_id' => 'loop',
        '_parent' => 'thesis_post_box_1348607689',
      ),
    ),
    'thesis_comments' => 
    array (
      'thesis_comments_1348716667' => 
      array (
        '_name' => 'Comments',
      ),
    ),
    'thesis_comment_form' => 
    array (
      'thesis_comment_form_1348843091' => 
      array (
        '_name' => 'Comment Form',
      ),
    ),
    'thesis_post_categories' => 
    array (
      'thesis_post_box_1348010947_thesis_post_categories' => 
      array (
        'html' => 'div',
        'intro' => 'in',
        'separator' => ',',
        '_id' => 'loop',
        '_parent' => 'thesis_post_box_1348010947',
      ),
      'thesis_post_box_1348607689_thesis_post_categories' => 
      array (
        'html' => 'div',
        'intro' => 'in',
        'separator' => ',',
        '_id' => 'loop',
        '_parent' => 'thesis_post_box_1348607689',
      ),
    ),
    'thesis_post_tags' => 
    array (
      'thesis_post_box_1348010947_thesis_post_tags' => 
      array (
        'intro' => 'Tagged as:',
        'separator' => ',',
        '_id' => 'loop',
        '_parent' => 'thesis_post_box_1348010947',
      ),
      'thesis_post_box_1348607689_thesis_post_tags' => 
      array (
        'intro' => 'Tagged as:',
        'separator' => ',',
        '_id' => 'loop',
        '_parent' => 'thesis_post_box_1348607689',
      ),
    ),
    'thesis_previous_post_link' => 
    array (
      'thesis_previous_post_link' => 
      array (
        'html' => 'p',
        'link' => 'custom',
        'text' => 'Previous',
      ),
    ),
    'thesis_next_post_link' => 
    array (
      'thesis_next_post_link' => 
      array (
        'html' => 'p',
        'link' => 'custom',
        'text' => 'Next',
      ),
    ),
    'thesis_text_box' => 
    array (
      'thesis_text_box_1350230891' => 
      array (
        '_id' => 'sidebar',
        '_name' => 'Sidebar Text Box',
      ),
      'thesis_text_box_1424230167' => 
      array (
        'text' => '  <script type="text/javascript">

	function MegaMenuMouseEnter(eventObject) {
		jQuery("#" + eventObject.data).addClass("mega-menu-visible");
	}

	function MegaMenuMouseLeave(eventObject) {
		jQuery("#" + eventObject.data).removeClass("mega-menu-visible");
	}

	function SetupMegaMenu(index, parent) {
		jQuery(parent).find("[class*=\'mega-menu-item-\']").each(function(index, element) {
			for (i = 0; i < element.classList.length; i++) {
				if (element.classList[i].substring(0,15) == "mega-menu-item-") {
					mega_menu_item_id = element.classList[i];
					jQuery(element).mouseenter(mega_menu_item_id, MegaMenuMouseEnter);
					jQuery(element).mouseleave(mega_menu_item_id, MegaMenuMouseLeave);
				}
			}
		});
	}

	jQuery(document).ready(
		function($) {
    			$(".mega-menu").each(SetupMegaMenu);
		}
	);
  </script>',
        'filter' => 
        array (
          'on' => true,
        ),
        'html' => 'none',
        '_name' => 'Mega Menu Script',
      ),
      'thesis_text_box_1394400451' => 
      array (
        'text' => '555-555-5555',
        'filter' => 
        array (
          'on' => true,
        ),
        '_name' => 'Contact Number',
      ),
      'thesis_text_box_1384348130' => 
      array (
        'text' => '[c2g_child_nav]',
        'class' => 'child-nav-container',
        '_name' => 'Child Links',
      ),
      'thesis_text_box_1384348097' => 
      array (
        'text' => '[c2g_breadcrumb]',
        'html' => 'none',
        '_name' => 'Breadcrumb',
      ),
      'thesis_text_box_1426161784' => 
      array (
        'text' => '<script type="text/javascript">
/*
* FlowType.JS v1.1
* Copyright 2013-2014, Simple Focus http://simplefocus.com/
*
* FlowType.JS by Simple Focus (http://simplefocus.com/)
* is licensed under the MIT License. Read a copy of the
* license in the LICENSE.txt file or at
* http://choosealicense.com/licenses/mit
*
* Thanks to Giovanni Difeterici (http://www.gdifeterici.com/)
*/

(function($) {
   $.fn.flowtype = function(options) {

// Establish default settings/variables
// ====================================
      var settings = $.extend({
         maximum   : 9999,
         minimum   : 1,
         maxFont   : 9999,
         minFont   : 1,
         fontRatio : 35
      }, options),

// Do the magic math
// =================
      changes = function(el) {
         var $el = $(el),
            elw = $el.width(),
            width = elw > settings.maximum ? settings.maximum : elw < settings.minimum ? settings.minimum : elw,
            fontBase = width / settings.fontRatio,
            fontSize = fontBase > settings.maxFont ? settings.maxFont : fontBase < settings.minFont ? settings.minFont : fontBase;
         $el.css(\'font-size\', fontSize + \'px\');
      };

// Make the magic visible
// ======================
      return this.each(function() {
      // Context for resize callback
         var that = this;
      // Make changes upon resize
         $(window).resize(function(){changes(that);});
      // Set changes on load
         changes(this);
      });
   };
}(jQuery));

   /* $(\'body\').flowtype(); */

    jQuery(\'body\').flowtype({
        minimum   : 500,
        maximum   : 1200,
        minFont   : 12,
        maxFont   : 20,
        fontRatio : 30
    });

    jQuery(\'.grid-item\').flowtype({
        minimum   : 500,
        maximum   : 1200,
        minFont   : 12,
        maxFont   : 20,
        fontRatio : 30
    });
</script>
',
        'filter' => 
        array (
          'on' => true,
        ),
        'html' => 'none',
        '_name' => 'FlowType Script',
      ),
    ),
    'thesis_comment_text' => 
    array (
      'thesis_comments_1348716667_thesis_comment_text' => 
      array (
        'class' => 'grt',
        '_parent' => 'thesis_comments_1348716667',
      ),
    ),
    'thesis_comments_nav' => 
    array (
      'thesis_comments_nav_1366218263' => 
      array (
        'class' => 'comment_nav_top',
        '_name' => 'Comment Nav Top',
      ),
      'thesis_comments_nav_1366218280' => 
      array (
        'class' => 'comment_nav_bottom',
        '_name' => 'Comment Nav Bottom',
      ),
    ),
    'thesis_wp_featured_image' => 
    array (
      'thesis_post_box_1348607689_thesis_wp_featured_image' => 
      array (
        'size' => 'medium',
        'link' => 
        array (
          'link' => false,
        ),
        '_id' => 'loop',
        '_parent' => 'thesis_post_box_1348607689',
      ),
      'thesis_post_box_1348010947_thesis_wp_featured_image' => 
      array (
        'size' => 'thumbnail',
        'alignment' => 'left',
        '_id' => 'loop',
        '_parent' => 'thesis_post_box_1348010947',
      ),
      'thesis_post_list_1425075570_thesis_wp_featured_image' => 
      array (
        'size' => 'thumbnail',
        '_parent' => 'thesis_post_list_1425075570',
      ),
    ),
    'thesis_post_thumbnail' => 
    array (
      'thesis_post_box_1348010947_thesis_post_thumbnail' => 
      array (
        'alignment' => 'left',
        '_id' => 'loop',
        '_parent' => 'thesis_post_box_1348010947',
      ),
      'thesis_post_box_1348607689_thesis_post_thumbnail' => 
      array (
        '_id' => 'loop',
        '_parent' => 'thesis_post_box_1348607689',
      ),
    ),
    'thesis_post_image' => 
    array (
      'thesis_post_box_1348607689_thesis_post_image' => 
      array (
        'link' => 
        array (
          'link' => false,
        ),
        '_id' => 'loop',
        '_parent' => 'thesis_post_box_1348607689',
      ),
      'thesis_post_box_1348010947_thesis_post_image' => 
      array (
        '_id' => 'loop',
        '_parent' => 'thesis_post_box_1348010947',
      ),
    ),
    'thesis_post_author_avatar' => 
    array (
      'thesis_post_box_1348010947_thesis_post_author_avatar' => 
      array (
        '_id' => 'loop',
        '_parent' => 'thesis_post_box_1348010947',
      ),
      'thesis_post_box_1348607689_thesis_post_author_avatar' => 
      array (
        '_id' => 'loop',
        '_parent' => 'thesis_post_box_1348607689',
      ),
    ),
    'thesis_post_author_description' => 
    array (
      'thesis_post_box_1348010947_thesis_post_author_description' => 
      array (
        '_id' => 'loop',
        '_parent' => 'thesis_post_box_1348010947',
      ),
      'thesis_post_box_1348607689_thesis_post_author_description' => 
      array (
        '_id' => 'loop',
        '_parent' => 'thesis_post_box_1348607689',
      ),
    ),
    'thesis_post_num_comments' => 
    array (
      'thesis_post_box_1348010947_thesis_post_num_comments' => 
      array (
        '_id' => 'loop',
        '_parent' => 'thesis_post_box_1348010947',
      ),
      'thesis_post_box_1348607689_thesis_post_num_comments' => 
      array (
        '_id' => 'loop',
        '_parent' => 'thesis_post_box_1348607689',
      ),
    ),
    'thesis_comment_avatar' => 
    array (
      'thesis_comments_1348716667_thesis_comment_avatar' => 
      array (
        '_id' => 'comments',
        '_parent' => 'thesis_comments_1348716667',
      ),
    ),
    'thesis_comment_date' => 
    array (
      'thesis_comments_1348716667_thesis_comment_date' => 
      array (
        '_id' => 'comments',
        '_parent' => 'thesis_comments_1348716667',
      ),
    ),
    'thesis_query_box' => 
    array (
      'thesis_query_box_1423002301' => 
      array (
      ),
    ),
    'thesis_post_content' => 
    array (
      'thesis_post_box_1348010947_thesis_post_content' => 
      array (
        'read_more' => '[read more...]',
        '_parent' => 'thesis_post_box_1348010947',
      ),
      'thesis_post_box_1348607689_thesis_post_content' => 
      array (
        'class' => 'the_content',
        '_parent' => 'thesis_post_box_1348607689',
      ),
    ),
    'thesis_post_list' => 
    array (
      'thesis_post_list_1425075570' => 
      array (
        'class' => 'filtered-products',
        '_name' => 'Post List',
      ),
    ),
  ),
  'vars' => 
  array (
    'var_1349039554' => 
    array (
      'name' => 'Spacing: Single',
      'ref' => 'x_single',
      'css' => '27px',
    ),
    'var_1349039577' => 
    array (
      'name' => 'Spacing: Half',
      'ref' => 'x_half',
      'css' => '14px',
    ),
    'var_1349039585' => 
    array (
      'name' => 'Spacing: Double',
      'ref' => 'x_double',
      'css' => '54px',
    ),
    'var_1349039761' => 
    array (
      'name' => 'Links',
      'ref' => 'links',
      'css' => '#DD0000',
    ),
    'var_1351010515' => 
    array (
      'name' => 'Clearfix',
      'ref' => 'z_clearfix',
      'css' => 'content: \\".\\"; display: block; height: 0; width: 0; clear: both; font-size: 0; visibility: hidden',
    ),
    'var_1360768628' => 
    array (
      'name' => 'Primary Text Color',
      'ref' => 'text1',
      'css' => '#333333',
    ),
    'var_1360768650' => 
    array (
      'name' => 'Secondary Text Color',
      'ref' => 'text2',
      'css' => '#888888',
    ),
    'var_1360768659' => 
    array (
      'name' => 'Color 1',
      'ref' => 'color1',
      'css' => '#DDDDDD',
    ),
    'var_1360768669' => 
    array (
      'name' => 'Color 2',
      'ref' => 'color2',
      'css' => '#3D9FD0',
    ),
    'var_1360768678' => 
    array (
      'name' => 'Color 3',
      'ref' => 'color3',
      'css' => '#FFFFFF',
    ),
    'var_1362537256' => 
    array (
      'name' => 'Font Size: Sub-headline',
      'ref' => 'f_subhead',
      'css' => '20px',
    ),
    'var_1362537274' => 
    array (
      'name' => 'Font Size: Text',
      'ref' => 'f_text',
      'css' => '16px',
    ),
    'var_1362537289' => 
    array (
      'name' => 'Font Size: Auxiliary',
      'ref' => 'f_aux',
      'css' => '13px',
    ),
    'var_1362580685' => 
    array (
      'name' => 'Line Height: Text',
      'ref' => 'h_text',
      'css' => '1.618',
    ),
    'var_1362580697' => 
    array (
      'name' => 'Line Height: Auxiliary Text',
      'ref' => 'h_aux',
      'css' => '23px',
    ),
    'var_1362588614' => 
    array (
      'name' => 'Spacing: 1.5',
      'ref' => 'x_3over2',
      'css' => '41px',
    ),
    'var_1362757553' => 
    array (
      'name' => 'Font: Primary',
      'ref' => 'font',
      'css' => '"Gill Sans", "Gill Sans MT", Calibri, "Trebuchet MS", sans-serif',
    ),
    'var_1362767543' => 
    array (
      'name' => 'Sidebar Spacing: Half',
      'ref' => 's_x_half',
      'css' => '10px',
    ),
    'var_1362767589' => 
    array (
      'name' => 'Sidebar Spacing: Single',
      'ref' => 's_x_single',
      'css' => '19px',
    ),
    'var_1362767601' => 
    array (
      'name' => 'Sidebar Spacing: 1.5',
      'ref' => 's_x_3over2',
      'css' => '29px',
    ),
    'var_1362768690' => 
    array (
      'name' => 'Sidebar Spacing: Double',
      'ref' => 's_x_double',
      'css' => '38px',
    ),
    'var_1363019458' => 
    array (
      'name' => 'Site Title Color',
      'ref' => 'title_color',
      'css' => '#333333',
    ),
    'var_1363458877' => 
    array (
      'name' => 'Site Title',
      'ref' => 'title',
      'css' => 'font-size: 42px;',
    ),
    'var_1363459110' => 
    array (
      'name' => 'Tagline',
      'ref' => 'tagline',
      'css' => 'font-size: 16px;
	color: #888888;',
    ),
    'var_1363467168' => 
    array (
      'name' => 'Nav Menu',
      'ref' => 'menu',
      'css' => 'font-size: 13px;
	line-height: 19px;',
    ),
    'var_1363467273' => 
    array (
      'name' => 'Sub-headline',
      'ref' => 'subhead',
      'css' => 'font-size: 20px;
	line-height: 32px;',
    ),
    'var_1363467831' => 
    array (
      'name' => 'Headline',
      'ref' => 'headline',
      'css' => 'font-family: Georgia, "Times New Roman", Times, serif;
	font-size: 26px;
	line-height: 39px;',
    ),
    'var_1363537291' => 
    array (
      'name' => 'Sidebar',
      'ref' => 'sidebar',
      'css' => 'font-size: 13px;
	line-height: 19px;',
    ),
    'var_1363621601' => 
    array (
      'name' => 'Blockquote',
      'ref' => 'blockquote',
      'css' => 'color: #888888;',
    ),
    'var_1363621659' => 
    array (
      'name' => 'Code',
      'ref' => 'code',
      'css' => 'font-family: Consolas, Monaco, Menlo, Courier, Verdana, sans-serif;',
    ),
    'var_1363621686' => 
    array (
      'name' => 'Pre-formatted Code',
      'ref' => 'pre',
      'css' => 'font-family: Consolas, Monaco, Menlo, Courier, Verdana, sans-serif;',
    ),
    'var_1363621701' => 
    array (
      'name' => 'Sidebar Heading',
      'ref' => 'sidebar_heading',
      'css' => 'font-size: 17px;
	line-height: 25px;',
    ),
    'var_1363633021' => 
    array (
      'name' => 'Headline Color',
      'ref' => 'headline_color',
      'css' => '#333333',
    ),
    'var_1363633037' => 
    array (
      'name' => 'Sub-headline Color',
      'ref' => 'subhead_color',
      'css' => '#333333',
    ),
    'var_1363989059' => 
    array (
      'name' => 'Author Avatar',
      'ref' => 'avatar',
      'css' => 'width: 62px;
	height: 62px;',
    ),
    'var_1364573035' => 
    array (
      'name' => 'Comment Avatar',
      'ref' => 'comment_avatar',
      'css' => 'width: 54px;
	height: 54px;',
    ),
    'var_1364921879' => 
    array (
      'name' => 'Author Description Avatar',
      'ref' => 'bio_avatar',
      'css' => 'width: 81px;
	height: 81px;',
    ),
    'var_1364931901' => 
    array (
      'name' => 'Pullquote',
      'ref' => 'pullquote',
      'css' => 'font-size: 26px;
	line-height: 37px;',
    ),
    'var_1366555361' => 
    array (
      'name' => 'Navigation Submenu',
      'ref' => 'submenu',
      'css' => '182px',
    ),
    'var_1421164558' => 
    array (
      'name' => 'Menu Color',
      'ref' => 'menu_color',
      'css' => '#EEEEEE',
    ),
    'var_1421177550' => 
    array (
      'name' => 'Menu Background',
      'ref' => 'menu_background',
      'css' => '#333333;',
    ),
    'var_1421177638' => 
    array (
      'name' => 'Header Color',
      'ref' => 'header_color',
      'css' => 'inherit',
    ),
    'var_1421177689' => 
    array (
      'name' => 'Header Background',
      'ref' => 'header_background',
      'css' => 'none',
    ),
    'var_1424378737' => 
    array (
      'name' => 'Thumbnail Height',
      'ref' => 'thumb_height',
      'css' => '150px',
    ),
    'var_1424378764' => 
    array (
      'name' => 'Thumbnail Width',
      'ref' => 'thumb_width',
      'css' => '150px',
    ),
    'var_1424897339' => 
    array (
      'name' => 'Intro Background',
      'ref' => 'intro_background',
      'css' => 'none',
    ),
  ),
  'templates' => 
  array (
    'home' => 
    array (
      'boxes' => 
      array (
        'thesis_html_body' => 
        array (
          0 => 'thesis_html_container_1421159875',
          1 => 'thesis_html_container_1421159324',
          2 => 'thesis_html_container_1348093642',
          3 => 'thesis_text_box_1426161784',
          4 => 'thesis_text_box_1424230167',
        ),
        'thesis_html_container_1421159875' => 
        array (
          0 => 'thesis_html_container_1424272580',
        ),
        'thesis_html_container_1424272580' => 
        array (
          0 => 'thesis_html_container_1424274879',
          1 => 'thesis_html_container_1424274935',
        ),
        'thesis_html_container_1424274879' => 
        array (
          0 => 'thesis_html_container_1421160415',
        ),
        'thesis_html_container_1421160415' => 
        array (
          0 => 'thesis_html_container_1348009564',
        ),
        'thesis_html_container_1424274935' => 
        array (
          0 => 'thesis_html_container_1424271399',
          1 => 'thesis_html_container_1424271461',
        ),
        'thesis_html_container_1424271399' => 
        array (
          0 => 'thesis_text_box_1394400451',
        ),
        'thesis_html_container_1424271461' => 
        array (
          0 => 'thesis_wp_nav_menu_1424271486',
        ),
        'thesis_html_container_1421159324' => 
        array (
          0 => 'thesis_html_container_1421106828',
        ),
        'thesis_html_container_1421106828' => 
        array (
          0 => 'thesis_wp_nav_menu_1348009742',
          1 => 'thesis_html_container_1424230803',
        ),
        'thesis_html_container_1424230803' => 
        array (
          0 => 'thesis_wp_widgets_1424183841',
        ),
        'thesis_html_container_1348093642' => 
        array (
          0 => 'thesis_html_container_1348009571',
          1 => 'thesis_html_container_1348009575',
        ),
        'thesis_html_container_1348009571' => 
        array (
          0 => 'thesis_html_container_1348010954',
          1 => 'thesis_html_container_1421160996',
        ),
        'thesis_html_container_1348010954' => 
        array (
          0 => 'thesis_wp_loop',
          1 => 'thesis_html_container_1348701154',
        ),
        'thesis_wp_loop' => 
        array (
          0 => 'thesis_post_box_1348010947',
        ),
        'thesis_post_box_1348010947' => 
        array (
          0 => 'thesis_html_container_1348886177',
          1 => 'thesis_post_box_1348010947_thesis_wp_featured_image',
          2 => 'thesis_post_box_1348010947_thesis_post_thumbnail',
          3 => 'thesis_post_box_1348010947_thesis_post_content',
          4 => 'thesis_post_box_1348010947_thesis_post_tags',
          5 => 'thesis_post_box_1348010947_thesis_post_num_comments',
        ),
        'thesis_html_container_1348886177' => 
        array (
          0 => 'thesis_post_box_1348010947_thesis_post_author_avatar',
          1 => 'thesis_post_box_1348010947_thesis_post_headline',
          2 => 'thesis_html_container_1348165494',
        ),
        'thesis_html_container_1348165494' => 
        array (
          0 => 'thesis_post_box_1348010947_thesis_post_author',
          1 => 'thesis_post_box_1348010947_thesis_post_date',
          2 => 'thesis_post_box_1348010947_thesis_post_edit',
          3 => 'thesis_post_box_1348010947_thesis_post_categories',
        ),
        'thesis_html_container_1421160996' => 
        array (
          0 => 'thesis_html_container_1348010964',
        ),
        'thesis_html_container_1348010964' => 
        array (
          0 => 'thesis_text_box_1350230891',
          1 => 'thesis_wp_widgets_1348079687',
        ),
        'thesis_html_container_1348009575' => 
        array (
          0 => 'thesis_html_container_1422909884',
        ),
        'thesis_html_container_1422909884' => 
        array (
          0 => 'thesis_wp_widgets_1391880855',
          1 => 'thesis_attribution',
        ),
      ),
    ),
    'archive' => 
    array (
      'boxes' => 
      array (
        'thesis_html_body' => 
        array (
          0 => 'thesis_html_container_1421159875',
          1 => 'thesis_html_container_1421159324',
          2 => 'thesis_html_container_1424916329',
          3 => 'thesis_html_container_1348009575',
          4 => 'thesis_text_box_1424230167',
        ),
        'thesis_html_container_1421159875' => 
        array (
          0 => 'thesis_html_container_1424272580',
        ),
        'thesis_html_container_1424272580' => 
        array (
          0 => 'thesis_html_container_1424274879',
          1 => 'thesis_html_container_1424274935',
        ),
        'thesis_html_container_1424274879' => 
        array (
          0 => 'thesis_html_container_1421160415',
        ),
        'thesis_html_container_1421160415' => 
        array (
          0 => 'thesis_html_container_1348009564',
        ),
        'thesis_html_container_1424274935' => 
        array (
          0 => 'thesis_html_container_1424271399',
          1 => 'thesis_html_container_1424271461',
        ),
        'thesis_html_container_1424271399' => 
        array (
          0 => 'thesis_text_box_1394400451',
        ),
        'thesis_html_container_1424271461' => 
        array (
          0 => 'thesis_wp_nav_menu_1424271486',
        ),
        'thesis_html_container_1421159324' => 
        array (
          0 => 'thesis_html_container_1422909884',
        ),
        'thesis_html_container_1422909884' => 
        array (
          0 => 'thesis_wp_nav_menu_1348009742',
        ),
        'thesis_html_container_1424916329' => 
        array (
          0 => 'thesis_html_container_1348009571',
        ),
        'thesis_html_container_1348009571' => 
        array (
          0 => 'thesis_html_container_1425074763',
          1 => 'thesis_html_container_1425074976',
        ),
        'thesis_html_container_1425074763' => 
        array (
          0 => 'thesis_wp_widgets_1425074917',
        ),
        'thesis_html_container_1425074976' => 
        array (
          0 => 'thesis_wp_loop',
        ),
        'thesis_wp_loop' => 
        array (
          0 => 'thesis_post_list_1425075570',
        ),
        'thesis_post_list_1425075570' => 
        array (
          0 => 'thesis_post_list_1425075570_thesis_wp_featured_image',
          1 => 'thesis_post_list_1425075570_thesis_post_headline',
        ),
        'thesis_html_container_1348009575' => 
        array (
          0 => 'thesis_html_container_1422910782',
          1 => 'thesis_attribution',
        ),
        'thesis_html_container_1422910782' => 
        array (
          0 => 'thesis_wp_widgets_1391880855',
        ),
      ),
    ),
    'custom_1348591137' => 
    array (
      'title' => 'Landing Page',
      'options' => 
      array (
        'thesis_html_body' => 
        array (
          'class' => 'landing',
        ),
      ),
      'boxes' => 
      array (
        'thesis_html_body' => 
        array (
          0 => 'thesis_html_container_1348093642',
        ),
        'thesis_html_container_1348093642' => 
        array (
          0 => 'thesis_html_container_1348009564',
          1 => 'thesis_html_container_1348010954',
          2 => 'thesis_html_container_1348009575',
        ),
        'thesis_html_container_1348009564' => 
        array (
          0 => 'thesis_site_title',
          1 => 'thesis_site_tagline',
        ),
        'thesis_html_container_1348010954' => 
        array (
          0 => 'thesis_wp_loop',
        ),
        'thesis_wp_loop' => 
        array (
          0 => 'thesis_post_box_1348607689',
        ),
        'thesis_post_box_1348607689' => 
        array (
          0 => 'thesis_html_container_1348886177',
          1 => 'thesis_post_box_1348607689_thesis_wp_featured_image',
          2 => 'thesis_post_box_1348607689_thesis_post_image',
          3 => 'thesis_post_box_1348607689_thesis_post_content',
        ),
        'thesis_html_container_1348886177' => 
        array (
          0 => 'thesis_post_box_1348607689_thesis_post_headline',
          1 => 'thesis_html_container_1348165494',
        ),
        'thesis_html_container_1348165494' => 
        array (
          0 => 'thesis_post_box_1348607689_thesis_post_edit',
        ),
        'thesis_html_container_1348009575' => 
        array (
          0 => 'thesis_attribution',
          1 => 'thesis_wp_admin',
        ),
      ),
    ),
    'single' => 
    array (
      'boxes' => 
      array (
        'thesis_html_body' => 
        array (
          0 => 'thesis_html_container_1421159875',
          1 => 'thesis_html_container_1421159324',
          2 => 'thesis_html_container_1348093642',
          3 => 'thesis_text_box_1424230167',
        ),
        'thesis_html_container_1421159875' => 
        array (
          0 => 'thesis_html_container_1424272580',
        ),
        'thesis_html_container_1424272580' => 
        array (
          0 => 'thesis_html_container_1424274879',
          1 => 'thesis_html_container_1424274935',
        ),
        'thesis_html_container_1424274879' => 
        array (
          0 => 'thesis_html_container_1421160415',
        ),
        'thesis_html_container_1421160415' => 
        array (
          0 => 'thesis_html_container_1348009564',
        ),
        'thesis_html_container_1424274935' => 
        array (
          0 => 'thesis_html_container_1424271399',
          1 => 'thesis_html_container_1424271461',
        ),
        'thesis_html_container_1424271399' => 
        array (
          0 => 'thesis_text_box_1394400451',
        ),
        'thesis_html_container_1424271461' => 
        array (
          0 => 'thesis_wp_nav_menu_1424271486',
        ),
        'thesis_html_container_1421159324' => 
        array (
          0 => 'thesis_html_container_1421106828',
        ),
        'thesis_html_container_1421106828' => 
        array (
          0 => 'thesis_wp_nav_menu_1348009742',
          1 => 'thesis_html_container_1424230803',
        ),
        'thesis_html_container_1424230803' => 
        array (
          0 => 'thesis_wp_widgets_1424183841',
        ),
        'thesis_html_container_1348093642' => 
        array (
          0 => 'thesis_html_container_1348009571',
          1 => 'thesis_html_container_1348009575',
        ),
        'thesis_html_container_1348009571' => 
        array (
          0 => 'thesis_html_container_1348010954',
          1 => 'thesis_html_container_1421160996',
        ),
        'thesis_html_container_1348010954' => 
        array (
          0 => 'thesis_wp_loop',
          1 => 'thesis_html_container_1348701154',
        ),
        'thesis_wp_loop' => 
        array (
          0 => 'thesis_post_box_1348607689',
          1 => 'thesis_html_container_1365640887',
        ),
        'thesis_post_box_1348607689' => 
        array (
          0 => 'thesis_html_container_1348886177',
          1 => 'thesis_html_container_1424374465',
          2 => 'thesis_post_box_1348607689_thesis_post_content',
        ),
        'thesis_html_container_1348886177' => 
        array (
          0 => 'thesis_post_box_1348607689_thesis_post_headline',
          1 => 'thesis_html_container_1348165494',
        ),
        'thesis_html_container_1348165494' => 
        array (
          0 => 'thesis_post_box_1348607689_thesis_post_author',
          1 => 'thesis_post_box_1348607689_thesis_post_edit',
        ),
        'thesis_html_container_1424374465' => 
        array (
          0 => 'thesis_post_box_1348607689_thesis_wp_featured_image',
        ),
        'thesis_html_container_1365640887' => 
        array (
          0 => 'thesis_comments_intro',
          1 => 'thesis_comments_nav_1366218263',
          2 => 'thesis_comments_1348716667',
          3 => 'thesis_comments_nav_1366218280',
          4 => 'thesis_comment_form_1348843091',
        ),
        'thesis_comments_1348716667' => 
        array (
          0 => 'thesis_html_container_1348841704',
          1 => 'thesis_comments_1348716667_thesis_comment_text',
          2 => 'thesis_html_container_1366209424',
        ),
        'thesis_html_container_1348841704' => 
        array (
          0 => 'thesis_comments_1348716667_thesis_comment_author',
          1 => 'thesis_comments_1348716667_thesis_comment_date',
        ),
        'thesis_html_container_1366209424' => 
        array (
          0 => 'thesis_comments_1348716667_thesis_comment_reply',
          1 => 'thesis_comments_1348716667_thesis_comment_edit',
        ),
        'thesis_comment_form_1348843091' => 
        array (
          0 => 'thesis_comment_form_1348843091_thesis_comment_form_title',
          1 => 'thesis_comment_form_1348843091_thesis_comment_form_cancel',
          2 => 'thesis_comment_form_1348843091_thesis_comment_form_name',
          3 => 'thesis_comment_form_1348843091_thesis_comment_form_email',
          4 => 'thesis_comment_form_1348843091_thesis_comment_form_url',
          5 => 'thesis_comment_form_1348843091_thesis_comment_form_comment',
          6 => 'thesis_comment_form_1348843091_thesis_comment_form_submit',
        ),
        'thesis_html_container_1421160996' => 
        array (
          0 => 'thesis_html_container_1348010964',
        ),
        'thesis_html_container_1348010964' => 
        array (
          0 => 'thesis_text_box_1350230891',
          1 => 'thesis_wp_widgets_1348079687',
        ),
        'thesis_html_container_1348009575' => 
        array (
          0 => 'thesis_html_container_1422910782',
          1 => 'thesis_attribution',
        ),
        'thesis_html_container_1422910782' => 
        array (
          0 => 'thesis_wp_widgets_1391880855',
        ),
      ),
    ),
    'page' => 
    array (
      'boxes' => 
      array (
        'thesis_html_body' => 
        array (
          0 => 'thesis_html_container_1421159875',
          1 => 'thesis_html_container_1421159324',
          2 => 'thesis_html_container_1424916329',
          3 => 'thesis_html_container_1348009575',
          4 => 'thesis_html_container_1425312086',
          5 => 'thesis_text_box_1426161784',
          6 => 'thesis_text_box_1424230167',
        ),
        'thesis_html_container_1421159875' => 
        array (
          0 => 'thesis_html_container_1424272580',
        ),
        'thesis_html_container_1424272580' => 
        array (
          0 => 'thesis_html_container_1424274879',
          1 => 'thesis_html_container_1424274935',
        ),
        'thesis_html_container_1424274879' => 
        array (
          0 => 'thesis_html_container_1421160415',
        ),
        'thesis_html_container_1421160415' => 
        array (
          0 => 'thesis_html_container_1348009564',
        ),
        'thesis_html_container_1424274935' => 
        array (
          0 => 'thesis_site_tagline',
        ),
        'thesis_html_container_1421159324' => 
        array (
          0 => 'thesis_html_container_1422909884',
        ),
        'thesis_html_container_1422909884' => 
        array (
          0 => 'thesis_wp_nav_menu_1348009742',
          1 => 'thesis_html_container_1424230803',
        ),
        'thesis_html_container_1424230803' => 
        array (
          0 => 'thesis_wp_widgets_1424183841',
        ),
        'thesis_html_container_1424916329' => 
        array (
          0 => 'thesis_html_container_1348009571',
        ),
        'thesis_html_container_1348009571' => 
        array (
          0 => 'thesis_html_container_1348010954',
          1 => 'thesis_html_container_1425055684',
          2 => 'thesis_html_container_1425055793',
          3 => 'thesis_html_container_1421160996',
        ),
        'thesis_html_container_1348010954' => 
        array (
          0 => 'thesis_wp_loop',
        ),
        'thesis_wp_loop' => 
        array (
          0 => 'thesis_post_box_1348607689',
        ),
        'thesis_post_box_1348607689' => 
        array (
          0 => 'thesis_post_box_1348607689_thesis_wp_featured_image',
          1 => 'thesis_post_box_1348607689_thesis_post_content',
        ),
        'thesis_html_container_1425055684' => 
        array (
          0 => 'thesis_wp_widgets_1425056136',
        ),
        'thesis_html_container_1425055793' => 
        array (
          0 => 'thesis_wp_widgets_1425056165',
        ),
        'thesis_html_container_1421160996' => 
        array (
          0 => 'thesis_html_container_1348010964',
        ),
        'thesis_html_container_1348010964' => 
        array (
          0 => 'thesis_text_box_1350230891',
          1 => 'thesis_wp_widgets_1348079687',
        ),
        'thesis_html_container_1348009575' => 
        array (
          0 => 'thesis_html_container_1422910782',
        ),
        'thesis_html_container_1422910782' => 
        array (
          0 => 'thesis_wp_widgets_1391880855',
        ),
        'thesis_html_container_1425312086' => 
        array (
          0 => 'thesis_attribution',
        ),
      ),
    ),
    'front' => 
    array (
      'boxes' => 
      array (
        'thesis_html_body' => 
        array (
          0 => 'thesis_html_container_1421159875',
          1 => 'thesis_html_container_1421159324',
          2 => 'thesis_html_container_1421159249',
          3 => 'thesis_wp_widgets_1391881268',
          4 => 'thesis_html_container_1424916329',
          5 => 'thesis_html_container_1348009575',
          6 => 'thesis_html_container_1425312086',
          7 => 'thesis_text_box_1426161784',
          8 => 'thesis_text_box_1424230167',
        ),
        'thesis_html_container_1421159875' => 
        array (
          0 => 'thesis_html_container_1424272580',
        ),
        'thesis_html_container_1424272580' => 
        array (
          0 => 'thesis_html_container_1424274879',
          1 => 'thesis_html_container_1424274935',
        ),
        'thesis_html_container_1424274879' => 
        array (
          0 => 'thesis_html_container_1421160415',
        ),
        'thesis_html_container_1421160415' => 
        array (
          0 => 'thesis_html_container_1348009564',
        ),
        'thesis_html_container_1424274935' => 
        array (
          0 => 'thesis_site_tagline',
        ),
        'thesis_html_container_1421159324' => 
        array (
          0 => 'thesis_html_container_1422909884',
        ),
        'thesis_html_container_1422909884' => 
        array (
          0 => 'thesis_wp_nav_menu_1348009742',
          1 => 'thesis_html_container_1424230803',
        ),
        'thesis_html_container_1424230803' => 
        array (
          0 => 'thesis_wp_widgets_1424183841',
        ),
        'thesis_html_container_1421159249' => 
        array (
          0 => 'thesis_html_container_1421160524',
        ),
        'thesis_html_container_1421160524' => 
        array (
          0 => 'thesis_wp_widgets_1426687710',
        ),
        'thesis_html_container_1424916329' => 
        array (
          0 => 'thesis_html_container_1348009571',
        ),
        'thesis_html_container_1348009571' => 
        array (
          0 => 'thesis_html_container_1348010954',
          1 => 'thesis_html_container_1425055684',
          2 => 'thesis_html_container_1425055793',
          3 => 'thesis_html_container_1421160996',
        ),
        'thesis_html_container_1348010954' => 
        array (
          0 => 'thesis_wp_loop',
        ),
        'thesis_wp_loop' => 
        array (
          0 => 'thesis_post_box_1348607689',
        ),
        'thesis_post_box_1348607689' => 
        array (
          0 => 'thesis_post_box_1348607689_thesis_wp_featured_image',
          1 => 'thesis_post_box_1348607689_thesis_post_content',
        ),
        'thesis_html_container_1425055684' => 
        array (
          0 => 'thesis_wp_widgets_1425056136',
        ),
        'thesis_html_container_1425055793' => 
        array (
          0 => 'thesis_wp_widgets_1425056165',
        ),
        'thesis_html_container_1421160996' => 
        array (
          0 => 'thesis_html_container_1348010964',
        ),
        'thesis_html_container_1348010964' => 
        array (
          0 => 'thesis_text_box_1350230891',
          1 => 'thesis_wp_widgets_1348079687',
        ),
        'thesis_html_container_1348009575' => 
        array (
          0 => 'thesis_html_container_1422910782',
        ),
        'thesis_html_container_1422910782' => 
        array (
          0 => 'thesis_wp_widgets_1391880855',
        ),
        'thesis_html_container_1425312086' => 
        array (
          0 => 'thesis_attribution',
        ),
      ),
    ),
    'custom_1422995660' => 
    array (
      'title' => 'Post with Comments',
      'boxes' => 
      array (
        'thesis_html_body' => 
        array (
          0 => 'thesis_html_container_1421159875',
          1 => 'thesis_html_container_1421159324',
          2 => 'thesis_html_container_1421159249',
          3 => 'thesis_html_container_1348093642',
        ),
        'thesis_html_container_1421159875' => 
        array (
          0 => 'thesis_html_container_1421160415',
        ),
        'thesis_html_container_1421160415' => 
        array (
          0 => 'thesis_html_container_1348009564',
        ),
        'thesis_html_container_1421159324' => 
        array (
          0 => 'thesis_html_container_1421106828',
        ),
        'thesis_html_container_1421106828' => 
        array (
          0 => 'thesis_wp_nav_menu_1348009742',
        ),
        'thesis_html_container_1421159249' => 
        array (
          0 => 'thesis_html_container_1421160524',
        ),
        'thesis_html_container_1421160524' => 
        array (
          0 => 'thesis_wp_widgets_1391881268',
        ),
        'thesis_html_container_1348093642' => 
        array (
          0 => 'thesis_html_container_1348009571',
          1 => 'thesis_html_container_1348009575',
        ),
        'thesis_html_container_1348009571' => 
        array (
          0 => 'thesis_html_container_1348010954',
          1 => 'thesis_html_container_1421160996',
        ),
        'thesis_html_container_1348010954' => 
        array (
          0 => 'thesis_wp_loop',
          1 => 'thesis_html_container_1348701154',
        ),
        'thesis_wp_loop' => 
        array (
          0 => 'thesis_post_box_1348607689',
          1 => 'thesis_html_container_1365640887',
        ),
        'thesis_post_box_1348607689' => 
        array (
          0 => 'thesis_html_container_1348886177',
          1 => 'thesis_post_box_1348607689_thesis_wp_featured_image',
          2 => 'thesis_post_box_1348607689_thesis_post_content',
        ),
        'thesis_html_container_1348886177' => 
        array (
          0 => 'thesis_post_box_1348607689_thesis_post_headline',
          1 => 'thesis_html_container_1348165494',
        ),
        'thesis_html_container_1348165494' => 
        array (
          0 => 'thesis_post_box_1348607689_thesis_post_author',
          1 => 'thesis_post_box_1348607689_thesis_post_edit',
        ),
        'thesis_html_container_1365640887' => 
        array (
          0 => 'thesis_comments_intro',
          1 => 'thesis_comments_nav_1366218263',
          2 => 'thesis_comments_1348716667',
          3 => 'thesis_comments_nav_1366218280',
          4 => 'thesis_comment_form_1348843091',
        ),
        'thesis_comments_1348716667' => 
        array (
          0 => 'thesis_html_container_1348841704',
          1 => 'thesis_comments_1348716667_thesis_comment_text',
          2 => 'thesis_html_container_1366209424',
        ),
        'thesis_html_container_1348841704' => 
        array (
          0 => 'thesis_comments_1348716667_thesis_comment_author',
          1 => 'thesis_comments_1348716667_thesis_comment_date',
        ),
        'thesis_html_container_1366209424' => 
        array (
          0 => 'thesis_comments_1348716667_thesis_comment_reply',
          1 => 'thesis_comments_1348716667_thesis_comment_edit',
        ),
        'thesis_comment_form_1348843091' => 
        array (
          0 => 'thesis_comment_form_1348843091_thesis_comment_form_title',
          1 => 'thesis_comment_form_1348843091_thesis_comment_form_cancel',
          2 => 'thesis_comment_form_1348843091_thesis_comment_form_name',
          3 => 'thesis_comment_form_1348843091_thesis_comment_form_email',
          4 => 'thesis_comment_form_1348843091_thesis_comment_form_url',
          5 => 'thesis_comment_form_1348843091_thesis_comment_form_comment',
          6 => 'thesis_comment_form_1348843091_thesis_comment_form_submit',
        ),
        'thesis_html_container_1421160996' => 
        array (
          0 => 'thesis_html_container_1348010964',
        ),
        'thesis_html_container_1348010964' => 
        array (
          0 => 'thesis_text_box_1350230891',
          1 => 'thesis_wp_widgets_1348079687',
        ),
        'thesis_html_container_1348009575' => 
        array (
          0 => 'thesis_attribution',
          1 => 'thesis_wp_admin',
        ),
      ),
      'options' => NULL,
    ),
    'fourohfour' => 
    array (
      'boxes' => 
      array (
        'thesis_html_body' => 
        array (
          0 => 'thesis_html_container_1421159875',
          1 => 'thesis_html_container_1421159324',
          2 => 'thesis_html_container_1348093642',
          3 => 'thesis_text_box_1424230167',
        ),
        'thesis_html_container_1421159875' => 
        array (
          0 => 'thesis_html_container_1424272580',
        ),
        'thesis_html_container_1424272580' => 
        array (
          0 => 'thesis_html_container_1424274879',
          1 => 'thesis_html_container_1424274935',
        ),
        'thesis_html_container_1424274879' => 
        array (
          0 => 'thesis_html_container_1421160415',
        ),
        'thesis_html_container_1421160415' => 
        array (
          0 => 'thesis_html_container_1348009564',
        ),
        'thesis_html_container_1424274935' => 
        array (
          0 => 'thesis_html_container_1424271399',
          1 => 'thesis_html_container_1424271461',
        ),
        'thesis_html_container_1424271399' => 
        array (
          0 => 'thesis_text_box_1394400451',
        ),
        'thesis_html_container_1424271461' => 
        array (
          0 => 'thesis_wp_nav_menu_1424271486',
        ),
        'thesis_html_container_1421159324' => 
        array (
          0 => 'thesis_html_container_1421106828',
        ),
        'thesis_html_container_1421106828' => 
        array (
          0 => 'thesis_wp_nav_menu_1348009742',
          1 => 'thesis_html_container_1424230803',
        ),
        'thesis_html_container_1424230803' => 
        array (
          0 => 'thesis_wp_widgets_1424183841',
        ),
        'thesis_html_container_1348093642' => 
        array (
          0 => 'thesis_html_container_1348009571',
          1 => 'thesis_html_container_1348009575',
        ),
        'thesis_html_container_1348009571' => 
        array (
          0 => 'thesis_wp_widgets_1424716390',
        ),
        'thesis_html_container_1348009575' => 
        array (
          0 => 'thesis_html_container_1422910782',
          1 => 'thesis_attribution',
        ),
        'thesis_html_container_1422910782' => 
        array (
          0 => 'thesis_wp_widgets_1391880855',
        ),
      ),
    ),
  ),
  '_design' => 
  array (
    'text1' => '333333',
    'color2' => '#3D9FD0',
    'font' => 
    array (
      'family' => 'gill_sans',
    ),
    'headline' => 
    array (
      'font-family' => 'georgia',
    ),
  ),
);
}
